#
# Auto-Generated file. Do not edit!
#

C_SRCS :=
CPP_SRCS :=
ASM_SRCS :=
OBJ_SRCS :=
C_DEPS :=
CPP_DEPS :=
ASM_DEPS :=
STARTO :=
OBJS :=
EXECUTABLES :=

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
RTThread/bsp-ls1x \
RTThread/bsp-ls1x/i2c \
RTThread/bsp-ls1x/spi \
RTThread/components/dfs/src \
RTThread/components/drivers/src \
RTThread/components/finsh \
RTThread/components/libc/compilers/newlib \
RTThread/components/libc/time \
RTThread/port \
RTThread/src \
image \
ls1x-drv \
ls1x-drv/ac97 \
ls1x-drv/can \
ls1x-drv/console \
ls1x-drv/fb \
ls1x-drv/fb/font \
ls1x-drv/gmac \
ls1x-drv/gpio \
ls1x-drv/i2c \
ls1x-drv/i2c/ads1015 \
ls1x-drv/i2c/gp7101 \
ls1x-drv/i2c/gt1151 \
ls1x-drv/i2c/mcp4725 \
ls1x-drv/i2c/pca9557 \
ls1x-drv/i2c/rx8010 \
ls1x-drv/nand \
ls1x-drv/pwm \
ls1x-drv/rtc \
ls1x-drv/spi \
ls1x-drv/spi/w25x40 \
ls1x-drv/spi/xpt2046 \
ls1x-drv/uart \
ls1x-drv/useful \
ls1x-drv/watchdog \
lvgl-7.0.1/porting \
lvgl-7.0.1/src/lv_core \
lvgl-7.0.1/src/lv_draw \
lvgl-7.0.1/src/lv_font \
lvgl-7.0.1/src/lv_hal \
lvgl-7.0.1/src/lv_misc \
lvgl-7.0.1/src/lv_themes \
lvgl-7.0.1/src/lv_widgets \
src \
src/lkdGui_source/port \
src/lkdGui_source/port/fontDriver \
src/lkdGui_source/source

