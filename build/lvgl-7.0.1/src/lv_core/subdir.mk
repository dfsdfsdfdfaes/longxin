#
# Auto-Generated file. Do not edit!
#

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../lvgl-7.0.1/src/lv_core/lv_debug.c \
../lvgl-7.0.1/src/lv_core/lv_disp.c \
../lvgl-7.0.1/src/lv_core/lv_group.c \
../lvgl-7.0.1/src/lv_core/lv_indev.c \
../lvgl-7.0.1/src/lv_core/lv_obj.c \
../lvgl-7.0.1/src/lv_core/lv_refr.c \
../lvgl-7.0.1/src/lv_core/lv_style.c

OBJS += \
./lvgl-7.0.1/src/lv_core/lv_debug.o \
./lvgl-7.0.1/src/lv_core/lv_disp.o \
./lvgl-7.0.1/src/lv_core/lv_group.o \
./lvgl-7.0.1/src/lv_core/lv_indev.o \
./lvgl-7.0.1/src/lv_core/lv_obj.o \
./lvgl-7.0.1/src/lv_core/lv_refr.o \
./lvgl-7.0.1/src/lv_core/lv_style.o

C_DEPS += \
./lvgl-7.0.1/src/lv_core/lv_debug.d \
./lvgl-7.0.1/src/lv_core/lv_disp.d \
./lvgl-7.0.1/src/lv_core/lv_group.d \
./lvgl-7.0.1/src/lv_core/lv_indev.d \
./lvgl-7.0.1/src/lv_core/lv_obj.d \
./lvgl-7.0.1/src/lv_core/lv_refr.d \
./lvgl-7.0.1/src/lv_core/lv_style.d

# Each subdirectory must supply rules for building sources it contributes
lvgl-7.0.1/src/lv_core/%.o: ../lvgl-7.0.1/src/lv_core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MIPS SDE Lite C Compiler'
	E:/LoongIDE/mips-2015.05/bin/mips-sde-elf-gcc.exe -mips32 -G0 -EL -msoft-float -DLS1B -DOS_RTTHREAD  -O1 -g -Wall -c -fmessage-length=0 -pipe -I"../" -I"../include" -I"../RTThread/include" -I"../RTThread/port/include" -I"../RTThread/port/mips" -I"../RTThread/components/finsh" -I"../RTThread/components/dfs/include" -I"../RTThread/components/drivers/include" -I"../RTThread/components/libc/time" -I"../RTThread/bsp-ls1x" -I"../ls1x-drv/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/ls1x-drv/include/i2c" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1/porting" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src/lkdGui_source/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

