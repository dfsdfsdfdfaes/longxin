#
# Auto-Generated file. Do not edit!
#

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../src/install_3th_libraries.c \
../src/rtt_lvgl_top.c \
../src/rgb_led.c \
../src/task1.c \
../src/buzzer.c \
../src/key.c \
../src/seg595.c \
../src/task2.c \
../src/task3.c \
../src/lm35_adc.c \
../src/task4.c \
../src/pwm_ic.c \
../src/heat_res.c \
../src/bh1750.c \
../src/bkrc_voice.c \
../src/task5.c \
../src/ultrasonic_ranging_drv.c \
../src/task6.c \
../src/task7.c \
../src/calculate.c \
../src/matrix.c \
../src/hc595.c \
../src/HX711.c

OBJS += \
./src/install_3th_libraries.o \
./src/rtt_lvgl_top.o \
./src/rgb_led.o \
./src/task1.o \
./src/buzzer.o \
./src/key.o \
./src/seg595.o \
./src/task2.o \
./src/task3.o \
./src/lm35_adc.o \
./src/task4.o \
./src/pwm_ic.o \
./src/heat_res.o \
./src/bh1750.o \
./src/bkrc_voice.o \
./src/task5.o \
./src/ultrasonic_ranging_drv.o \
./src/task6.o \
./src/task7.o \
./src/calculate.o \
./src/matrix.o \
./src/hc595.o \
./src/HX711.o

C_DEPS += \
./src/install_3th_libraries.d \
./src/rtt_lvgl_top.d \
./src/rgb_led.d \
./src/task1.d \
./src/buzzer.d \
./src/key.d \
./src/seg595.d \
./src/task2.d \
./src/task3.d \
./src/lm35_adc.d \
./src/task4.d \
./src/pwm_ic.d \
./src/heat_res.d \
./src/bh1750.d \
./src/bkrc_voice.d \
./src/task5.d \
./src/ultrasonic_ranging_drv.d \
./src/task6.d \
./src/task7.d \
./src/calculate.d \
./src/matrix.d \
./src/hc595.d \
./src/HX711.d

# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MIPS SDE Lite C Compiler'
	E:/LoongIDE/mips-2015.05/bin/mips-sde-elf-gcc.exe -mips32 -G0 -EL -msoft-float -DLS1B -DOS_RTTHREAD  -O1 -g -Wall -c -fmessage-length=0 -pipe -I"../" -I"../include" -I"../RTThread/include" -I"../RTThread/port/include" -I"../RTThread/port/mips" -I"../RTThread/components/finsh" -I"../RTThread/components/dfs/include" -I"../RTThread/components/drivers/include" -I"../RTThread/components/libc/time" -I"../RTThread/bsp-ls1x" -I"../ls1x-drv/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/ls1x-drv/include/i2c" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1/porting" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src/lkdGui_source/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

