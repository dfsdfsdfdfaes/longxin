#
# Auto-Generated file. Do not edit!
#

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../src/lkdGui_source/port/fontDriver/ASCII_16_32.c \
../src/lkdGui_source/port/fontDriver/ASCII_5_7.c \
../src/lkdGui_source/port/fontDriver/ASCII_64_64.c \
../src/lkdGui_source/port/fontDriver/ASCII_6_12.c \
../src/lkdGui_source/port/fontDriver/ASCII_8_16.c \
../src/lkdGui_source/port/fontDriver/GB2312_12_12.c \
../src/lkdGui_source/port/fontDriver/GB2312_16_16.c \
../src/lkdGui_source/port/fontDriver/GB2312_32_32.c

OBJS += \
./src/lkdGui_source/port/fontDriver/ASCII_16_32.o \
./src/lkdGui_source/port/fontDriver/ASCII_5_7.o \
./src/lkdGui_source/port/fontDriver/ASCII_64_64.o \
./src/lkdGui_source/port/fontDriver/ASCII_6_12.o \
./src/lkdGui_source/port/fontDriver/ASCII_8_16.o \
./src/lkdGui_source/port/fontDriver/GB2312_12_12.o \
./src/lkdGui_source/port/fontDriver/GB2312_16_16.o \
./src/lkdGui_source/port/fontDriver/GB2312_32_32.o

C_DEPS += \
./src/lkdGui_source/port/fontDriver/ASCII_16_32.d \
./src/lkdGui_source/port/fontDriver/ASCII_5_7.d \
./src/lkdGui_source/port/fontDriver/ASCII_64_64.d \
./src/lkdGui_source/port/fontDriver/ASCII_6_12.d \
./src/lkdGui_source/port/fontDriver/ASCII_8_16.d \
./src/lkdGui_source/port/fontDriver/GB2312_12_12.d \
./src/lkdGui_source/port/fontDriver/GB2312_16_16.d \
./src/lkdGui_source/port/fontDriver/GB2312_32_32.d

# Each subdirectory must supply rules for building sources it contributes
src/lkdGui_source/port/fontDriver/%.o: ../src/lkdGui_source/port/fontDriver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MIPS SDE Lite C Compiler'
	E:/LoongIDE/mips-2015.05/bin/mips-sde-elf-gcc.exe -mips32 -G0 -EL -msoft-float -DLS1B -DOS_RTTHREAD  -O1 -g -Wall -c -fmessage-length=0 -pipe -I"../" -I"../include" -I"../RTThread/include" -I"../RTThread/port/include" -I"../RTThread/port/mips" -I"../RTThread/components/finsh" -I"../RTThread/components/dfs/include" -I"../RTThread/components/drivers/include" -I"../RTThread/components/libc/time" -I"../RTThread/bsp-ls1x" -I"../ls1x-drv/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/ls1x-drv/include/i2c" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1/porting" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src/lkdGui_source/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

