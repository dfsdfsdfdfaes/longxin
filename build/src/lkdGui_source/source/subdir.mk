#
# Auto-Generated file. Do not edit!
#

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../src/lkdGui_source/source/lkdGuiBasic.c \
../src/lkdGui_source/source/lkdGuiColour.c \
../src/lkdGui_source/source/lkdGuiControl.c \
../src/lkdGui_source/source/lkdGuiFont.c \
../src/lkdGui_source/source/lkdGuiMenu.c \
../src/lkdGui_source/source/lkdGuiWin.c

OBJS += \
./src/lkdGui_source/source/lkdGuiBasic.o \
./src/lkdGui_source/source/lkdGuiColour.o \
./src/lkdGui_source/source/lkdGuiControl.o \
./src/lkdGui_source/source/lkdGuiFont.o \
./src/lkdGui_source/source/lkdGuiMenu.o \
./src/lkdGui_source/source/lkdGuiWin.o

C_DEPS += \
./src/lkdGui_source/source/lkdGuiBasic.d \
./src/lkdGui_source/source/lkdGuiColour.d \
./src/lkdGui_source/source/lkdGuiControl.d \
./src/lkdGui_source/source/lkdGuiFont.d \
./src/lkdGui_source/source/lkdGuiMenu.d \
./src/lkdGui_source/source/lkdGuiWin.d

# Each subdirectory must supply rules for building sources it contributes
src/lkdGui_source/source/%.o: ../src/lkdGui_source/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MIPS SDE Lite C Compiler'
	E:/LoongIDE/mips-2015.05/bin/mips-sde-elf-gcc.exe -mips32 -G0 -EL -msoft-float -DLS1B -DOS_RTTHREAD  -O1 -g -Wall -c -fmessage-length=0 -pipe -I"../" -I"../include" -I"../RTThread/include" -I"../RTThread/port/include" -I"../RTThread/port/mips" -I"../RTThread/components/finsh" -I"../RTThread/components/dfs/include" -I"../RTThread/components/drivers/include" -I"../RTThread/components/libc/time" -I"../RTThread/bsp-ls1x" -I"../ls1x-drv/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/ls1x-drv/include/i2c" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/lvgl-7.0.1/porting" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src/lkdGui_source/include" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/src" -I"D:/Loong_test/LS1B_RTT_LVGL7_ALL_DEMO/include" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

