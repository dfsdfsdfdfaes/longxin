/**
 * @file 龙芯主函数
 * @author 张霖鑫
 * @brief
 * @version 0.1
 * @date 2023-10-8
 *
 * @copyright Copyright (c) 2024
 *
 */

#include <time.h>
#include "mips.h"
#include "rtthread.h"
#include "rgb_led.h"
#include "buzzer.h"
#include "key.h"
#include "seg595.h"
#include "ls1x_i2c_bus.h"
#include "ls1b.h"
#include "lm35_adc.h"
#include "heat_res.h"
#include "bh1750.h"
#include "bkrc_voice.h"
#include "ultrasonic_ranging_drv.h"
#include "HX711.h"
//-------------------------------------------------------------------------------------------------
// BSP
//-------------------------------------------------------------------------------------------------

#include "bsp.h"

#if defined(BSP_USE_FB)
#include "ls1x_fb.h"
#ifdef XPT2046_DRV
char LCD_display_mode[] = LCD_800x480;
#elif defined(GT1151_DRV)
char LCD_display_mode[] = LCD_800x480;
#else
#error "在bsp.h中选择配置 XPT2046_DRV 或者 GT1151_DRV"
"XPT2046_DRV:  用于800*480 横屏的触摸屏."
    "GT1151_DRV:   用于480*800 竖屏的触摸屏."
    "如果都不选择, 注释掉本 error 信息, 然后自定义: LCD_display_mode[]"
#endif
#endif

//-------------------------------------------------------------------------------------------------
// Simple demo of task
//-------------------------------------------------------------------------------------------------
#include "my_def.h"
#include "gui_layout.h"
#include "rtt_lvgl_top.h"

static rt_thread_t m_demo_thread = NULL;

static void demo_thread(void *arg)
{
  unsigned int tickcount;

  for (;;)
  {
    tickcount = rt_tick_get();

    rt_kprintf("tick count = %i\r\n", tickcount);

    rt_thread_delay(500);
  }
}

/*----------------------------------------------------------------
 * 函数名称：Hardware_Init
 * 功    能：硬件初始化
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：
 *--------------------------------------------------------------*/
void Hardware_Init(void)
{
  RGB_Init();          // RGB三色灯初始化
  Buzzer_Init();       // 蜂鸣器初始化
  Key_IsrInit();       // 按键初始化
  HC595_Init();        // 数码管初始化
  Show_ADS1015_Info(); // 显示ADS1015配置
  HeatRes_Init();      // 加热电阻初始化
  BH1750_Init();       // BH1750初始化
  UART4_Init();        // 串口4初始化
  PWM2Timer_Init();    // PWM定时器2初始化
  Ultrasonic_Init();   // 超声波初始化
  Matrix_init();       // 点阵模块初始化
  HX711_Init();        // 称重模块初始化
}

//-------------------------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  rt_kprintf("\r\nWelcome to RT-Thread.\r\n\r\n");

  ls1x_drv_init(); /* Initialize device drivers */

  rt_ls1x_drv_init(); /* Initialize device drivers for RTT */

  install_3th_libraries(); /* Install 3th libraies */

  Hardware_Init();

  rtt_lvgl_init();
  delay_ms(700);
  // BKRCspeak_TTS("你好,欢迎使用合肥职业技术学院龙芯操作系统");
  /*
   * Task initializing...
   */
  m_demo_thread = rt_thread_create("demothread",
                                   demo_thread,
                                   NULL,     // arg
                                   1024 * 4, // statck size
                                   11,       // priority
                                   10);      // slice ticks

  if (m_demo_thread == NULL)
  {
    rt_kprintf("create demo thread fail!\r\n");
  }
  else
  {
    rt_thread_startup(m_demo_thread);
  }

  /*
   * Finsh as another thread...
   */
  return 0;
}

/*
 * @@ End
 */
