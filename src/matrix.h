/*
 * matrix.h
 *
 * created: 2021/5/23
 *  author:
 */

#ifndef _MATRIX_H
#define _MATRIX_H

#define RED 1
#define GREEN 2
#define BLUE 3

extern unsigned int Second;

void Matrix_init(void);
void LED_ALL_ON(void);
void LED_ALL_OFF(void);
void Dis_H(void);
void Dis_L(void);
void Dis_Char(unsigned char *data);
void Dis_Dynamic(unsigned char *data, unsigned int len);
void Dis_Roll(unsigned char *data, unsigned int len);
void Dis_1_Roll(unsigned char *data, unsigned int len);
void Dis_2_Roll(unsigned char *data, unsigned int len);
void Dis_Num(unsigned char data);
void Matrix_8x8(unsigned char (*data)[8]);

#endif // _MATRIX_H
