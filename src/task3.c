
//-----------------------------------------------------------
// 必要的头文件
//-----------------------------------------------------------
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "task3.h"
#include "gui_layout.h"
#include <stdio.h>
#include "rgb_led.h"

//-----------------------------------------------------------
// 全屏显示颜色子线程（三级）控制块和启动标志
//-----------------------------------------------------------
static rt_thread_t fullscr_color_thread = RT_NULL;
static rt_bool_t fullscr_color_started = RT_FALSE;

static lv_obj_t *gled_slider; // 控制绿灯的滑杆
static lv_obj_t *gled_label;  // 滑杆旁的文字标签
static int16_t gled_value;    // 滑块的值

//-----------------------------------------------------------
// 滑块控制LED亮度子线程（子任务3.3）控制块、启动标志和入口函数
//-----------------------------------------------------------
static rt_thread_t led_slider_thread = RT_NULL;
static rt_bool_t led_slider_started = RT_FALSE;
// 控制LED亮度的定时器控制块、启动标志和回调函数
//-----------------------------------------------------------
static rt_timer_t led_luminance_timer = RT_NULL;
static rt_bool_t led_luminance_started = RT_FALSE;

/*---------------------------------------------------------------------
 * 函数名称：gled_slider_event_cb
 * 功    能：滑块事件回调函数
 * 入口参数：按照事件回调函数格式
 * 返 回 值：无
 * 说明描述：获取滑块的值并显示“绿xx%”
 *-------------------------------------------------------------------*/
static void gled_slider_event_cb(lv_obj_t *obj, lv_event_t event)
{
    static char label_buf[8]; // 存放标签内容的缓冲区

    if (event == LV_EVENT_VALUE_CHANGED)
    {
        // 当滑块值有变化时获取其值并显示在标签上
        gled_value = lv_slider_get_value(obj);
        snprintf(label_buf, sizeof(label_buf),
                 ""
                 "\xE7\xBB\xBF" /*绿色*/ "%d%%",
                 gled_value);
        lv_label_set_text(gled_label, label_buf);
    }
}

/*----------------------------------------------------------------------
 * 函数名称：led_slider_gui
 * 功    能：生成滑杆的界面
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：在led_slider_thread_entry函数中调用
 *--------------------------------------------------------------------*/
void led_slider_gui(void)
{
    // 创建一个控制绿灯亮度的滑杆
    gled_slider = lv_slider_create(lv_scr_act(), NULL);
    // 设置滑杆类型
    lv_slider_set_type(gled_slider, LV_SLIDER_TYPE_NORMAL);
    // 滑杆宽度和高度（长一点高一点便于手指接触滑动）
    lv_obj_set_width(gled_slider, 320);
    lv_obj_set_height(gled_slider, 30);
    // 放在屏幕右半区
    lv_obj_align(gled_slider, NULL, LV_ALIGN_IN_RIGHT_MID, -120, 0);

    // 滑杆初始默认值
    lv_slider_set_value(gled_slider, 0, 0);
    // 取值范围0~100
    lv_slider_set_range(gled_slider, 0, 100);

    // 发生滑动的回调事件
    lv_obj_set_event_cb(gled_slider, gled_slider_event_cb);

    // 再创建一个标签，用于显示滑块的值
    gled_label = lv_label_create(lv_scr_act(), NULL);
    // 放在滑杆右侧
    lv_obj_align(gled_label, gled_slider, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    // 标签初始文字内容：绿 0%
    lv_label_set_text(gled_label, ""
                                  "\xE7\xBB\xBF" /*绿色*/ "  0%");
}

/*----------------------------------------------------------------------
 * 函数名称：led_slider_thread_entry
 * 功    能：滑杆控制LED亮度子线程入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：生成滑杆界面
 *--------------------------------------------------------------------*/
static void led_slider_thread_entry(void *arg)
{
    led_slider_gui(); // 生成滑杆界面

    while (1) // 阻塞时间短一点，滑动更流畅，但线程开销增大
    {
        rt_thread_mdelay(2);
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：led_slider_thread_init
 * 功    能：滑杆控制LED亮度子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int led_slider_thread_init(void)
{
    // 创建子线程，带图像显示的线程栈空间略大一点，优先级最低
    led_slider_thread = rt_thread_create("led_slider_thread",
                                         led_slider_thread_entry,
                                         RT_NULL,
                                         THREAD_STACK_SIZE * 2,
                                         THREAD_PRIORITY - 1,
                                         THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (led_slider_thread != RT_NULL)
    {
        rt_thread_startup(led_slider_thread);
        rt_kprintf("led_slider_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create led_slider_thread failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------
 * 函数名称：led_luminance_timer_cb
 * 功    能：LED亮度变化定时器回调函数
 * 入口参数：按照定时器回调函数格式
 * 返 回 值：无
 * 说明描述：1ms执行一次，参照呼吸灯的控制
 *---------------------------------------------------------------*/
static void led_luminance_timer_cb(void *arg)
{
    static rt_uint16_t pwm_value = 0; // 占空比大小计数

    pwm_value++; // 累加占空比大小计数

    // 这里把PWM周期设成10ms，滑块值0~9亮度为0档，10~19亮度为1档，以此类推
    // PWM周期不能过长，否则LED就会闪起来了
    if (pwm_value <= gled_value / 10) // 亮的时间
    {
        GREEN_ON;
    }
    else if (pwm_value < 10) // 灭的时间
    {
        GREEN_OFF;
    }
    else
    {
        pwm_value = 0; // 准备下一个PWM周期
    }
}

/*----------------------------------------------------------------
 * 函数名称：led_luminance_timer_init
 * 功    能：LED亮度变化定时器的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：周期性软件定时器，1ms
 *---------------------------------------------------------------*/
int led_luminance_timer_init(void)
{
    // 创建定时器
    led_luminance_timer = rt_timer_create("led_luminance_timer",
                                          led_luminance_timer_cb,
                                          NULL,
                                          1,
                                          RT_TIMER_FLAG_PERIODIC |
                                              RT_TIMER_FLAG_SOFT_TIMER);

    // 创建成功则启动定时器
    if (led_luminance_timer != RT_NULL)
    {
        rt_timer_start(led_luminance_timer);
        rt_kprintf("led_luminance_timer running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create led_luminance_timer failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------------
 * 函数名称：img_btnm_thread_entry
 * 功    能：生成图像按钮矩阵
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：需要用挂起维持该线程
 *--------------------------------------------------------------------*/
static void img_btnm_thread_entry(void *arg)
{
    img_btnm_gui(); // 生成图像按钮矩阵

    while (1)
    {
        rt_thread_mdelay(20);
    }
}

/*----------------------------------------------------------------
 * 函数名称：fullscr_color_thread_entry
 * 功    能：全屏显示颜色子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：依次全屏显示红、绿、蓝，间隔1s，随后该线程自动退出
 *--------------------------------------------------------------*/
static void fullscr_color_thread_entry(void *arg)
{
    lv_style_t style_rect; // 定义一个用于矩形的样式

    rt_uint8_t count; // 有限循环的计数

    // 创建一个全屏的矩形对象
    lv_obj_t *rect = lv_obj_create(lv_scr_act(), NULL);
    lv_obj_set_size(rect, LV_HOR_RES, LV_VER_RES);

    // 初始化矩形框样式
    lv_style_init(&style_rect);

    // 3次循环用来依次切换
    for (count = 0; count < 3; count++)
    {
        switch (count)
        {
        case 0: // 填充红色
            lv_style_set_bg_color(&style_rect, LV_STATE_DEFAULT, LV_COLOR_RED);
            lv_obj_add_style(rect, LV_OBJ_PART_MAIN, &style_rect);
            break;
        case 1: // 填充绿色
            lv_style_set_bg_color(&style_rect, LV_STATE_DEFAULT, LV_COLOR_GREEN);
            lv_obj_add_style(rect, LV_OBJ_PART_MAIN, &style_rect);
            break;
        case 2: // 填充蓝色
            lv_style_set_bg_color(&style_rect, LV_STATE_DEFAULT, LV_COLOR_BLUE);
            lv_obj_add_style(rect, LV_OBJ_PART_MAIN, &style_rect);
            break;
        default:
            break;
        }
        rt_thread_mdelay(1000); // 1s切换一次
    }

    // 循环结束，线程会自动退出，删除样式和矩形对象即可
    lv_obj_reset_style_list(rect, LV_OBJ_PART_MAIN);
    lv_obj_del(rect);
}

/*----------------------------------------------------------------
 * 函数名称：fullscr_color_thread_init
 * 功    能：全屏显示颜色子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int fullscr_color_thread_init(void)
{
    // 创建子线程，带图像显示的线程栈空间略大一点，优先级比主线程低
    fullscr_color_thread = rt_thread_create("fullscr_color_thread",
                                            fullscr_color_thread_entry,
                                            RT_NULL,
                                            THREAD_STACK_SIZE * 2,
                                            THREAD_PRIORITY - 1,
                                            THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (fullscr_color_thread != RT_NULL)
    {
        rt_thread_startup(fullscr_color_thread);
        rt_kprintf("fullscr_color_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create fullscr_color_thread failed.\n");
        return -1;
    }
}

//-----------------------------------------------------------
// 任务3子线程（二级）控制块
//-----------------------------------------------------------
rt_thread_t task3_thread = RT_NULL;

//---------------------------------------------------------------------
// 代码清单7：修改后的task3_thread_entry函数
//---------------------------------------------------------------------

/*---------------------------------------------------------------------
 * 函数名称：task3_thread_entry
 * 功    能：任务3子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务3下的各子任务（线程）
 *-------------------------------------------------------------------*/
static void task3_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK3) // 主按钮为任务3
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1: // 子任务3.1按钮
                if (!fullscr_color_started)
                {
                    fullscr_color_thread_init();     // 启动全屏颜色子线程
                    fullscr_color_started = RT_TRUE; // 全屏颜色子线程标志置位
                }
                break;

            case SUB_TASK_2: // 子任务3.2按钮
                if (!led_slider_started && !led_luminance_started)
                {
                    led_slider_thread_init();
                    led_luminance_timer_init();
                    led_slider_started = RT_TRUE;
                    led_luminance_started = RT_TRUE;
                }
                break;

            case SUB_TASK_EXIT:            // 任务3退出按钮
                if (fullscr_color_started) // 子任务3.1启动标志为真则退出
                {
                    // 全屏颜色子线程执行完会自动退出，所以不必手动删除
                    fullscr_color_started = RT_FALSE;
                }
                if (led_luminance_started)
                {
                    rt_timer_delete(led_luminance_timer);
                    led_luminance_started = RT_FALSE;
                }
                if (led_slider_started)
                {
                    lv_obj_del(gled_slider);
                    lv_obj_del(gled_label);
                    gled_value = 0;
                    GREEN_OFF;
                    rt_thread_delete(led_slider_thread);
                    led_slider_started = RT_FALSE;
                }
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task3_thread_init
 * 功    能：任务3子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task3_thread_init(void)
{
    // 创建任务3子线程，栈空间加大点，为后续图形显示的子任务预留
    task3_thread = rt_thread_create("task3_thread",
                                    task3_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务3子线程
    if (task3_thread != RT_NULL)
    {
        rt_thread_startup(task3_thread);
        rt_kprintf("task3_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task3_thread failed.\n");
        return -1;
    }
}
