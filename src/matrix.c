/*
 * matrix.c
 *
 * created: 2021/5/23
 *  author:
 */
 #include "matrix.h"
 #include "hc595.h"
 #include "ls1x_rtc.h"
#include "rtthread.h"
 //行扫描数据数组
 unsigned short Data_H[32] = {  0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //第一个行管HC595
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};//第二个行管HC595

 //列扫描数据数组
 unsigned short Data_L[32] = {  0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //第一个行管HC595
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};//第二个行管HC595
/*************************************************************************************
 **函数功能：初始化IO口
 **说明：
 **************************************************************************************/
 void Matrix_init(void)
 {
    HC595_init();   //HC595IO初始化
    LED_ALL_OFF();

    //Matrix_Test();
 }

 /*************************************************************************************
 **函数功能：点阵上的点全部被点亮
 **说明：
 **************************************************************************************/
 void LED_ALL_ON(void)
 {
    HC595_Send_Data(0x00);
    HC595_Send_Data(0x00);
    HC595_Send_Data(0xff);
    HC595_Send_Data(0xff);
    HC595_Out();
 }

 /*************************************************************************************
 **函数功能：点阵上的点全部被熄灭
 **说明：
 **************************************************************************************/
 void LED_ALL_OFF(void)
 {
    HC595_Send_Data(0x00);
    HC595_Send_Data(0x00);
    HC595_Send_Data(0x00);
    HC595_Send_Data(0x00);
    HC595_Out();
 }

 /*************************************************************************************
 **函数功能：行扫描（行检测）
 **说明：
 **************************************************************************************/
 void Dis_H(void)
 {
     unsigned char i;
     for(i = 0; i < 16; i++)
     {
        HC595_Send_Data(0x00);
        HC595_Send_Data(0x00);
        HC595_Send_Data(Data_H[i+16]);
        HC595_Send_Data(Data_H[i]);
        HC595_Out();
        delay_ms(300);
     }
 }

 /*************************************************************************************
 **函数功能：列扫描（列检测）
 **说明：
 **************************************************************************************/
 void Dis_L(void)
 {
     unsigned char i;
     for(i = 0; i < 16; i++)
     {
        HC595_Send_Data(~Data_L[i+16]);
        HC595_Send_Data(~Data_L[i]);
        HC595_Send_Data(0xff);
        HC595_Send_Data(0xff);
        HC595_Out();
        delay_ms(300);
     }
 }
/*************************************************************************************
 **函数功能：测试点阵
 **说明：
 **************************************************************************************/
 void Matrix_Test(void)
 {
     LED_ALL_ON();

     delay_ms(1000);
     LED_ALL_OFF();

     delay_ms(1000);
     Dis_H();

     Dis_L();
 }

 /*************************************************************************************
 **函数功能：静态显示
 **说明：可显示一个汉字和字符，图片等
 **参数：@unsigned char *data  -- 数据
 **************************************************************************************/
 void Dis_Char(unsigned char *data)
 {
    unsigned char i,j = 0;
    for(i = 0; i < 16; i++)//扫描16行
    {
        HC595_Send_Data(data[j+1]);
        HC595_Send_Data(data[j]);
        HC595_Send_Data(Data_H[i+16]);
        HC595_Send_Data(Data_H[i]);
        HC595_Out();
        j += 2;
    }
    j = 0;
 }

 /*************************************************************************************
 **函数功能：静态显示
 **说明：可显示一个汉字和字符，图片等
 **参数：@unsigned char *data  -- 数据
 **************************************************************************************/
unsigned char NUM[];
 void Dis_Num(unsigned char data)
 {
    	unsigned char i,j = 0;

    	j=data/10*16;
        for(i = 0; i < 8; i++)//扫描左8列
        {
            HC595_Send_Data(~Data_L[i+8]);
            HC595_Send_Data(~Data_L[i]);
            HC595_Send_Data(~NUM[j+1]);
            HC595_Send_Data(~NUM[j]);
            HC595_Out();
            j += 2;
        }
		j=(data%10)*16;
        for(i = 8; i < 16; i++)//扫描右8列
        {
            HC595_Send_Data(~Data_L[i+16]);
            HC595_Send_Data(~Data_L[i]);
            HC595_Send_Data(~NUM[j+1]);
            HC595_Send_Data(~NUM[j]);
            HC595_Out();
            j += 2;
        }
 }


/*************************************************************************************
 **函数功能：动态显示
 **说明：可连续显示汉字和字符，图片等
 **参数：@unsigned char *data  -- 数据
         @unsigned char len -- 数据帧长度
 **************************************************************************************/
 void Dis_Dynamic(unsigned char *data, unsigned int len)
 {
    static unsigned int cnt = 0;
    unsigned char i,k;
    unsigned char j = 0;
    for(k = 0; k < 200; k++)//控制变换字符的速度
    {
        for(i = 0; i < 16; i++)//扫描16行
        {
            HC595_Send_Data(data[j+1+cnt]);
            HC595_Send_Data(data[j+cnt]);
            HC595_Send_Data(Data_H[i+16]);
            HC595_Send_Data(Data_H[i]);
            HC595_Out();
            j += 2;
        }
        j = 0;
    }
    cnt += 32;
    if(cnt > len - 32)
        cnt=0;
 }

 /*************************************************************************************
 **函数功能：从下往上滚动显示
 **参数：@unsigned char *data  -- 数据
         @unsigned char len -- 数据帧长度
 **************************************************************************************/
 void Dis_Roll(unsigned char *data, unsigned int len)
 {
    static unsigned int cnt = 0;
    unsigned char i,k;
    unsigned char j = 0;
    for(k = 0; k < 100; k++)//控制变换字符的速度
    {
        for(i = 0; i < 16; i++)//扫描16行
        {
            HC595_Send_Data(data[j+1+cnt]);
            HC595_Send_Data(data[j+cnt]);
            HC595_Send_Data(Data_H[i+16]);
            HC595_Send_Data(Data_H[i]);
            HC595_Out();
            j += 2;
        }
        j = 0;
    }
    cnt += 2;
    if(cnt > len - 32)
        cnt=0;
 }

 /*************************************************************************************
 **函数功能：从右往左滚动显示
 **参数：@unsigned char *data  -- 数据
         @unsigned char len -- 数据帧长度
 **************************************************************************************/
 void Dis_1_Roll(unsigned char *data, unsigned int len)
 {
    static unsigned int cnt = 0;
    unsigned char i,k;
    unsigned char j = 0;
    for(k = 0; k < 20; k++)//控制变换字符的速度
    {
        for(i = 0; i < 16; i++)//扫描16列
        {
            HC595_Send_Data(~Data_L[i+16]);
            HC595_Send_Data(~Data_L[i]);
            HC595_Send_Data(~data[j+1+cnt]);
            HC595_Send_Data(~data[j+cnt]);
            HC595_Out();
            j += 2;
        }
        j = 0;
    }
    cnt += 2;
    if(cnt > len-32)
        cnt=0;
 }
 /*************************************************************************************
 **函数功能：从右往左滚动显示
 **参数：@unsigned char *data  -- 数据
         @unsigned char len -- 数据帧长度
 **************************************************************************************/
 void Dis_2_Roll(unsigned char *data, unsigned int len)
 {
    static unsigned int cnt = 0;
    unsigned char i,k;
    unsigned char j = 0;
    for(k = 0; k < 150; k++)//控制变换字符的速度
    {
        for(i = 0; i < 16; i++)//扫描16列
        {
            HC595_Send_Data(~Data_L[i+16]);
            HC595_Send_Data(~Data_L[i]);
            HC595_Send_Data(~data[j+1+cnt]);
            HC595_Send_Data(~data[j+cnt]);
            HC595_Out();
            j += 2;
        }
        j = 0;
    }
    cnt += 32;
    if(cnt > len-32)
        cnt=0;
 }

