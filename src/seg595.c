
//-------------------------------------------------------------
// 必要的头文件
//-------------------------------------------------------------
#include "ls1b.h"
#include "mips.h"
#include "ls1b_gpio.h"
#include "seg595.h"

//-------------------------------------------------------------
// 全局变量
//-------------------------------------------------------------
const uchar num_code[15] =
    { // 0~f的共阴段码
        0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f,
        0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};

// 4个数码管的显示段码，初始为全灭
uchar disp_tab[4] = {0x00, 0x00, 0x00, 0x00};

/* -----------------------------------------------------------
 * 函数名称：HC595_Init
 * 函数功能：数码管驱动初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：SI、RCK、SCK均为输出
 * ---------------------------------------------------------*/
void HC595_Init(void)
{
    gpio_enable(39, DIR_OUT); // SI
    gpio_enable(38, DIR_OUT); // RCK
    gpio_enable(55, DIR_OUT); // SCK
}

/*-------------------------------------------------------------
 * 函数名称：HC595_SendData
 * 函数功能：发送位码和段码数据
 * 入口参数：pos --- 位码（见.h文件的宏）
 *           dat --- 段码
 * 返 回 值：无
 * 说    明：高片的595控制位码，要先发；段码后发，发送逻辑一致
 * ----------------------------------------------------------*/
void HC595_SendData(uchar pos, uchar dat)
{
    uchar temp; // 临时变量
    uchar i;    // 循环控制下标

    temp = pos; // 先准备位码，发给高片的74595
    for (i = 0; i < 8; i++)
    {
        temp = pos & 0x80; // 取出最高位
        if (temp)          // 是1就给SI高电平
            HC595_SI(1);
        else // 是0就给SI低电平
            HC595_SI(0);

        pos <<= 1; // 位码左移1位，准备发送下一位

        HC595_SCK(0); // 以下时序产生SCK上升沿，完成1位发送
        delay_us(1);
        HC595_SCK(1);
        delay_us(1);
    }

    temp = dat; // 再准备段码，发给低片的595，过程同上
    for (i = 0; i < 8; i++)
    {
        temp = dat & 0x80;
        if (temp)
            HC595_SI(1);
        else
            HC595_SI(0);

        dat <<= 1;

        HC595_SCK(0);
        delay_us(1);
        HC595_SCK(1);
        delay_us(1);
    }

    HC595_RCK(0); // 两码就绪，并行输出并锁存
    delay_us(1);
    HC595_RCK(1);
}

/*----------------------------------------------------------------
 * 函数名称：HC595_Display
 * 函数功能：数码管显示函数
 * 入口参数：start --- 起始位置，1代表左边第一位
 *			 num --- 显示位数（1~4）
 * 返 回 值：无
 * 说    明：在定时器里调用，比如2ms刷新一位显示
 * -------------------------------------------------------------*/
void HC595_Display(uchar start, uchar num)
{
    static uchar i = 0; // 下标索引

    switch (start + i) // 在当前数码管上显示内容
    {
    case 1:
        HC595_SendData(SEL_SEG1, disp_tab[0]);
        break;
    case 2:
        HC595_SendData(SEL_SEG2, disp_tab[1]);
        break;
    case 3:
        HC595_SendData(SEL_SEG3, disp_tab[2]);
        break;
    case 4:
        HC595_SendData(SEL_SEG4, disp_tab[3]);
        break;
    default:
        break;
    }

    if (++i == num) // 选择下一个数码管
        i = 0;
}
