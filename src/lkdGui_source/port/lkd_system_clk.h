/*
 * lkd_system_clk.h
 *
 * created: 2022/7/13
 *  author: 
 */

#ifndef _LKD_SYSTEM_CLK_H
#define _LKD_SYSTEM_CLK_H
uint32_t GetTimer1Tick(void);
uint32_t GetTimer1IntervalTick(uint32_t beginTick);

#endif // _LKD_SYSTEM_CLK_H

