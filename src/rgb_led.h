//------------------------------------------
// 代码清单1：rgb_led.h
//------------------------------------------

#ifndef RGB_LED_H
#define RGB_LED_H

#include "ls1b.h"
#include "ls1b_gpio.h"

//------------------------------------------
// 端口宏定义
//------------------------------------------
#define RED_LED 49
#define GREEN_LED 53
#define BLUE_LED 52

//------------------------------------------
// 函数宏定义（亮/灭/切换）
//------------------------------------------
#define RED_ON gpio_write(RED_LED, 1)
#define GREEN_ON gpio_write(GREEN_LED, 1)
#define BLUE_ON gpio_write(BLUE_LED, 1)
#define RED_OFF gpio_write(RED_LED, 0)
#define GREEN_OFF gpio_write(GREEN_LED, 0)
#define BLUE_OFF gpio_write(BLUE_LED, 0)
#define RED_TOG gpio_write(RED_LED, !gpio_read(RED_LED))
#define GREEN_TOG gpio_write(GREEN_LED, !gpio_read(GREEN_LED))
#define BLUE_TOG gpio_write(BLUE_LED, !gpio_read(BLUE_LED))
#define RGB_ON    \
    {             \
        RED_ON;   \
        GREEN_ON; \
        BLUE_ON;  \
    }
#define RGB_OFF    \
    {              \
        RED_OFF;   \
        GREEN_OFF; \
        BLUE_OFF;  \
    }

//------------------------------------------
// 函数声明
//------------------------------------------
void RGB_Init(void);

#endif
