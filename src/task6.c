
//----------------------------------------------------------------
// 必要的头文件
//----------------------------------------------------------------
#include <stdio.h>
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "gui_layout.h"
#include "task6.h"
#include "ultrasonic_ranging_drv.h"
#include "HX711.h"

//----------------------------------------------------------------
// 全局变量
//----------------------------------------------------------------
static lv_obj_t *dist_label; // 距离标签
static lv_obj_t *weight_label; // 重量标签

//----------------------------------------------------------------
// 超声波测距子线程（子任务6.1）控制块、启动标志和入口函数
//----------------------------------------------------------------
static rt_thread_t distance_thread = RT_NULL;
static rt_bool_t distance_started = RT_FALSE;
static rt_thread_t weight_thread = RT_NULL;
static rt_bool_t weight_started = RT_FALSE;
/*----------------------------------------------------------------
 * 函数名称：distance_thread_entry
 * 功    能：超声波测距子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：准备显示标签，每1s测量并显示一次距离
 *---------------------------------------------------------------*/
static void distance_thread_entry(void *arg)
{
    char dist_str[50];           // 距离标签的字符串
    static rt_uint8_t count = 0; // 控制显示刷新的计数变量

    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备显示距离的标签
    dist_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(dist_label, 250);
    lv_obj_align(dist_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 80);
    lv_obj_add_style(dist_label, LV_LABEL_PART_MAIN, &style_label);

    while (1)
    {
        // 启动测量并留出测量时间
        Ultrasonic_Start();
        rt_thread_mdelay(100);

        // 得到测量数据并对异常的处理
        if (distance <= 0)
            distance = last_distance;
        last_distance = distance;

        // 通过计数变量控制每1s变化1次显示，代替rt_thread_mdelay(1000)的长时挂起
        if (++count == 10)
        {
            // 把距离格式化成字符串
            snprintf(dist_str, sizeof(dist_str),
                     ""
                     "\xE8\xB7\x9D" /*距*/ ""
                     "\xE7\xA6\xBB" /*离*/ ": %.1f cm",
                     distance);
            // 通过标签显示距离
            lv_label_set_align(dist_label, LV_LABEL_ALIGN_LEFT);
            lv_label_set_text(dist_label, dist_str);
            count = 0;
        }
    }
}

/*----------------------------------------------------------------
 * 函数名称：distance_thread_init
 * 功    能：超声波测距子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int distance_thread_init(void)
{
    // 创建子线程，栈空间不大，优先级比子任务线程低
    distance_thread = rt_thread_create("distance_thread",
                                       distance_thread_entry,
                                       RT_NULL,
                                       THREAD_STACK_SIZE * 2,
                                       THREAD_PRIORITY - 1,
                                       THREAD_TIMESLICE);

    // 创建成功就启动该线程
    if (distance_thread != RT_NULL)
    {
        rt_thread_startup(distance_thread);
        rt_kprintf("distance_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create distance_thread failed.\n");
        return -1;
    }
}


/*----------------------------------------------------------------
 * 函数名称：weight_thread_entry
 * 功    能：称重子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：准备显示标签，每1s测量并显示一次重量
 *---------------------------------------------------------------*/
static void weight_thread_entry(void *arg)
{
    long Weight_Value;
    char buf[50];       // 重量标签的字符串
    static rt_uint8_t count = 0;    // 控制显示刷新的计数变量
    
    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备显示重量的标签
    weight_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(weight_label, 250);
    lv_obj_align(weight_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 80);
    lv_obj_add_style(weight_label, LV_LABEL_PART_MAIN, &style_label);
    
    Get_Maopi();
    
    while(1)
    {
        
        Weight_Value = Get_Weight();
        rt_thread_mdelay(100);
        
        /*if(Weight_Value <= 0)
        {
            Weight_Value = last_Weight_Value;
        }
        last_Weight_Value = Weight_Value;*/
        
        // 通过计数变量控制每1s变化1次显示，代替rt_thread_mdelay(1000)的长时挂起
        if (++count == 10)
        {
            // 把重量格式化成字符串
            snprintf(buf, sizeof(buf),
                     ""
                     "\xE9\x87\x8D" /*重*/ ""
                     "\xE9\x87\x8F" /*量*/ ": %.1f g",
                     Weight_Value / 10.0);
            // 通过标签显示重量
            lv_label_set_align(weight_label, LV_LABEL_ALIGN_LEFT);
            lv_label_set_text(weight_label, buf);
            count = 0;
        }
    }
}


/*----------------------------------------------------------------
 * 函数名称：weight_thread_init
 * 功    能：称重模块子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int weight_thread_init(void)
{
    // 创建子线程，栈空间不大，优先级比子任务线程低
    weight_thread = rt_thread_create("weight_thread",
                                       weight_thread_entry,
                                       RT_NULL,
                                       THREAD_STACK_SIZE * 2,
                                       THREAD_PRIORITY - 1,
                                       THREAD_TIMESLICE);

    // 创建成功就启动该线程
    if (weight_thread != RT_NULL)
    {
        rt_thread_startup(weight_thread);
        rt_kprintf("weight_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create weight_thread failed.\n");
        return -1;
    }
}

//---------------------------------------------------------------------
// 任务6子线程（二级）控制块
//---------------------------------------------------------------------
rt_thread_t task6_thread = RT_NULL;

/*---------------------------------------------------------------------
 * 函数名称：task6_thread_entry
 * 功    能：任务6子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务6下的各子任务（线程）
 *-------------------------------------------------------------------*/
static void task6_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK6) // 主按钮为任务6
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1: // 子任务6.1按钮
                if (!distance_started)
                {
                    distance_thread_init();     // 启动超声波测距子线程
                    distance_started = RT_TRUE; // 超声波测距标志置位
                }
                break;
                
            case SUB_TASK_2: // 子任务6.2按钮
                if (!weight_started)
                {
                    weight_thread_init();     // 启动超声波测距子线程
                    weight_started = RT_TRUE; // 超声波测距标志置位
                }
                break;

            case SUB_TASK_EXIT:       // 任务6退出按钮
                if (distance_started) // 子任务6.1启动标志为真则退出
                {
                    lv_obj_del(dist_label);
                    rt_thread_delete(distance_thread);
                    distance_started = RT_FALSE;
                }
                if (weight_started) // 子任务6.1启动标志为真则退出
                {
                    lv_obj_del(weight_label);
                    rt_thread_delete(weight_thread);
                    weight_started = RT_FALSE;
                }
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task6_thread_init
 * 功    能：任务6子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task6_thread_init(void)
{
    // 创建任务6子线程，栈空间加大点，为后续图形显示的子任务预留
    task6_thread = rt_thread_create("task6_thread",
                                    task6_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务6子线程
    if (task6_thread != RT_NULL)
    {
        rt_thread_startup(task6_thread);
        rt_kprintf("task6_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task6_thread failed.\n");
        return -1;
    }
}
