/**
 * @file 计算器算法函数
 * @author 张霖鑫
 * @brief
 * @version 0.1
 * @date 2024-01-15
 *
 * @copyright Copyright (c) 2024
 *
 */
//-----------------------------------------------------------
// 必要的头文件
//-----------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "calculate.h"

/*----------------------------------------------------------------------
 * 函数名称：is_operator_symbol
 * 功    能：判断字符是否是运算符
 * 入口参数：ch --- 符号
 * 返 回 值：1 --- 是运算符
 *           0 --- 不是运算符
 * 说明描述：计算器UI界面上没有括号，所以只判断 + - * /
 *--------------------------------------------------------------------*/
int is_operator_symbol(char ch)
{
    if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
        return 1;
    return 0;
}

/*----------------------------------------------------------------------
 * 函数名称：power
 * 功    能：算一个整数n次方
 * 入口参数：base --- 整数
 *           times --- 几次方
 * 返 回 值：取得的浮点数
 * 说明描述：连乘即可
 *--------------------------------------------------------------------*/
long int power(int base, int times)
{
    long ret = 1;
    int i;

    for (i = 0; i < times; i++)
        ret *= base;

    return ret;
}

/*----------------------------------------------------------------------
 * 函数名称：get_data
 * 功    能：从字符串中提取出浮点数
 * 入口参数：*str --- 字符串
 *           beg --- 起始位置
 *           end --- 结束位置
 * 返 回 值：取得的浮点数
 * 说明描述：1. 找到小数点，整数和小数分开处理
 *           2. 字符0~9的ASCII码是0x30~0x39，采取 ^0x30的方式可以实现
 *              数字字符转换成对应数值
 *--------------------------------------------------------------------*/
double get_data(char *str, int beg, int end)
{
    int i, k;
    double ret = 0.0f;
    double integer = 0.0f;
    double decimal = 0.0f;
    double temp;
    i = beg;

    // 取整数部分
    while (str[i] != '.' && i < end)
    {
        integer *= 10;
        integer += str[i] ^ 0x30;
        i++;
    }

    // 取小数部分
    if (str[i] == '.')
    {
        i++;
        k = 1;
        while (i < end)
        {
            temp = str[i] ^ 0x30;
            temp /= power(10, k);
            decimal += temp;
            k++;
            i++;
        }
    }

    // 整数部分加上小数部分
    ret = integer + decimal;
    return ret;
}

/*----------------------------------------------------------------------
 * 函数名称：calculate
 * 功    能：表达式计算函数
 * 入口参数：*expression --- 字符串类型的表达式
 * 返 回 值：计算结果
 * 说明描述：基于逆波兰算法
 *--------------------------------------------------------------------*/
double calculate(char *expression)
{
    unsigned int i, j, k;
    unsigned int symbol_len = 0;       // 运算符栈的长度
    unsigned int datas_len = 0;        // 操作数栈的长度
    unsigned int symbol_index_len = 0; // 运算符的个数
    double left_data, right_data;      // 左右操作数
    double temp, ret;                  // 处理过程的中间值

    // 申请操作数栈空间和运算符栈空间
    double *datas = (double *)malloc(sizeof(double) * MAX_SIZE);
    char *symbols = (char *)malloc(sizeof(char *) * SYMBOL_SIZE);

    // 定义运算符结构体
    oper_symbol *symbols_index = (oper_symbol *)malloc(sizeof(oper_symbol) * SYMBOL_SIZE);

    // 申请失败则结束返回
    if (datas == NULL || symbols == NULL || symbols_index == NULL)
    {
        printf("memory allocation failed.\n");
        return 0;
    }

    // 遍历中缀表达式，遇到运算符则更新运算符结构体里的信息
    for (i = 0; i < strlen(expression); i++)
    {
        if (is_operator_symbol(expression[i]))
        {
            symbols_index[symbol_index_len].oper = expression[i];
            symbols_index[symbol_index_len].index = i;
            symbol_index_len++;
        }
    }

    // 根据运算符提取两边的数据进行计算
    for (j = 0, i = 0; j < symbol_index_len; j++)
    {
        // 取左操作数
        if (datas_len == 0)
            left_data = get_data(expression, i, symbols_index[j].index);
        else
            left_data = datas[--datas_len];

        // 取右操作数
        if (j + 1 == symbol_index_len)
            right_data = get_data(expression, symbols_index[j].index + 1, strlen(expression));
        else
            right_data = get_data(expression, symbols_index[j].index + 1, symbols_index[j + 1].index);

        // 如果运算符是*和/，先计算结果再保存
        if (symbols_index[j].oper == '*' || symbols_index[j].oper == '/')
        {
            switch (symbols_index[j].oper)
            {
            case '*':
                temp = left_data * right_data;
                break;
            case '/':
                temp = left_data / right_data;
                break;
            }
            datas[datas_len++] = temp;
        }
        // 否则（+和-）将左右操作数直接保存
        else
        {
            datas[datas_len++] = left_data;
            datas[datas_len++] = right_data;
            // 记录+和-运算符的个数
            symbols[symbol_len++] = symbols_index[j].oper;
        }
    }

    // 开始从后向前进行计算
    k = symbol_len;

    // 直到操作符为空为止
    while (k)
    {
        // 取出左右两个操作数
        left_data = datas[--datas_len];
        right_data = datas[--datas_len];
        k = symbol_len - 1;
        switch (symbols[k])
        {
        case '+':
            temp = right_data + left_data;
            break;
        case '-':
            temp = right_data - left_data;
            break;
        }
        datas[datas_len++] = temp;
        symbol_len--;
    }

    // 释放栈空间，返回结果
    ret = datas[0];
    free(datas);
    free(symbols);
    free(symbols_index);
    return ret;
}
