

//----------------------------------------------------------------------
// 必要的头文件
//----------------------------------------------------------------------
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "gui_layout.h"
#include "task5.h"
#include "bkrc_voice.h"
#include "rgb_led.h"
#include "buzzer.h"

#define BUZZ_NONE 0
#define BUZZ_TICK 1
#define BUZZ_HELP 2
//----------------------------------------------------------------------
// 全局变量
//----------------------------------------------------------------------
static lv_obj_t *voice_label;                 // 语音识别的文字标签
static lv_obj_t *spk_btn;                     // 用来播报的按钮
static lv_obj_t *spk_label;                   // 播报按钮上的文字标签
static rt_bool_t btn_clicked_flag = RT_FALSE; // 按钮点击标志
static rt_uint8_t buzz_flag = BUZZ_NONE;      // 鸣响类型标志

//----------------------------------------------------------------
// 代码清单2：蜂鸣器定时器代码块
//----------------------------------------------------------------

//----------------------------------------------------------------
// 无源蜂鸣器定时器控制块和启动标志
//----------------------------------------------------------------
static rt_timer_t buzzer_timer = RT_NULL;
static rt_bool_t buzzer_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：buzzer_timer_cb
 * 功    能：无源蜂鸣器定时器回调函数
 * 入口参数：按照定时器回调函数格式
 * 返 回 值：无
 * 说明描述：控制无源蜂鸣器的IO口1ms变化一次电平，即对应500Hz脉冲，
 *           若是普通鸣响，响0.3s，关0.3s
 *           若是求救信号，3短（0.1s），3长（0.3s），3短（0.1s）
 *---------------------------------------------------------------*/
static void buzzer_timer_cb(void *arg)
{
    // 定时次数计数，count1用于普通鸣响，count2用于求救鸣响
    static rt_uint16_t count1 = 0, count2 = 0;

    if (buzz_flag == BUZZ_TICK) // 普通鸣响的定时计数
    {
        count2 = 0;        // 求救鸣响计数清0
        count1++;          // 累加计数
        if (count1 <= 300) // 前300ms，产生脉冲发出嘀的声音
        {
            BUZZER_TOG;
        }
        else if (count1 <= 600) // 后300ms，关闭蜂鸣器
        {
            BUZZER_OFF;
        }
        else // 清零，后续重复上述过程
        {
            count1 = 0;
        }
    }
    else if (buzz_flag == BUZZ_HELP) // 求救鸣响的定时计数
    {
        count1 = 0;           // 普通鸣响计数清0
        count2++;             // 累加计数
        switch (count2 / 100) // 100ms判断一次该响还是该停
        {
        // 前3声短的鸣响
        case 0:
        case 2:
        case 4:
        // 中间3声长的鸣响
        case 6:
        case 7:
        case 8:
        case 12:
        case 13:
        case 14:
        case 18:
        case 19:
        case 20:
        // 后3声短的鸣响
        case 24:
        case 26:
        case 28:
            BUZZER_TOG;
            break;

        // 前3声短的不响
        case 1:
        case 3:
        case 5:
        // 中间3声长的不响
        case 9:
        case 10:
        case 11:
        case 15:
        case 16:
        case 17:
        case 21:
        case 22:
        case 23:
        // 后3声短的不响
        case 25:
        case 27:
        case 29:
            BUZZER_OFF;
            break;

        // 发完一组求救信号，间隔0.5s，再开始下一组
        case 34:
            count2 = 0;
            break;

        default:
            break;
        }
    }
}

/*----------------------------------------------------------------
 * 函数名称：buzzer_timer_init
 * 功    能：蜂鸣器定时器的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：周期性软件定时器，1ms
 *---------------------------------------------------------------*/
int buzzer_timer_init(void)
{
    // 创建定时器
    buzzer_timer = rt_timer_create("buzzer_timer",
                                   buzzer_timer_cb,
                                   NULL,
                                   1,
                                   RT_TIMER_FLAG_PERIODIC |
                                       RT_TIMER_FLAG_SOFT_TIMER);

    // 创建成功则启动定时器
    if (buzzer_timer != RT_NULL)
    {
        rt_timer_start(buzzer_timer);
        rt_kprintf("buzzer_timer running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create buzzer_timer failed.\n");
        return -1;
    }
}
//----------------------------------------------------------------
// RGB闪烁灯子线程控制块和启动标志
//----------------------------------------------------------------
static rt_thread_t blink_thread = RT_NULL;
static rt_bool_t blink_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：blink_thread_entry
 * 功    能：RGB闪烁灯子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：红绿蓝黄青紫，间隔0.3s
 *---------------------------------------------------------------*/
static void blink_thread_entry(void *arg)
{
    rt_uint8_t count = 0; // 计数变量

    while (1)
    {
        count++;       // 计数累加
        switch (count) // 根据计数值决定RGB的状态
        {
        case 1:
            RED_ON;
            break;
        case 2:
            RED_OFF;
            break;
        case 3:
            GREEN_ON;
            break;
        case 4:
            GREEN_OFF;
            break;
        case 5:
            BLUE_ON;
            break;
        case 6:
            BLUE_OFF;
            break;
        case 7:
            RED_ON;
            GREEN_ON;
            break; // 红绿同时亮为黄
        case 8:
            RGB_OFF;
            break;
        case 9:
            GREEN_ON;
            BLUE_ON;
            break; // 绿蓝同时亮为青
        case 10:
            RGB_OFF;
            break;
        case 11:
            RED_ON;
            BLUE_ON;
            break; // 红蓝同时亮为紫
        case 12:
            RGB_OFF;
            count = 0;
            break;
        default:
            break;
        }
        rt_thread_mdelay(300); // 线程中要用挂起延时
    }
}

/*----------------------------------------------------------------
 * 函数名称：blink_thread_init
 * 功    能：RGB闪烁灯子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int blink_thread_init(void)
{
    // 创建子线程，栈空间不大，优先级比子任务线程低
    blink_thread = rt_thread_create("blink_thread",
                                    blink_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE,
                                    THREAD_PRIORITY,
                                    THREAD_TIMESLICE);

    // 创建成功就启动该线程
    if (blink_thread != RT_NULL)
    {
        rt_thread_startup(blink_thread);
        rt_kprintf("blink_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create blink_thread failed.\n");
        return -1;
    }
}
//----------------------------------------------------------------------
// 语音识别子线程（子任务5.1）控制块和启动标志
//----------------------------------------------------------------------
static rt_thread_t voice_thread = RT_NULL;
static rt_bool_t voice_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：spk_btn_event_cb
 * 功    能：单击播报按钮所执行的操作
 * 入口参数：按照回调函数的参数格式
 * 返 回 值：无
 * 说明描述：调用播报字符串的函数
 *--------------------------------------------------------------------*/
static void spk_btn_event_cb(lv_obj_t *obj, lv_event_t event)
{
    // 如果按钮按下且点击标志为0，这样可避免连触
    if (event == LV_EVENT_CLICKED && !btn_clicked_flag)
    {
        btn_clicked_flag = RT_TRUE;              // 按钮点击标志置位
        BKRCspeak_TTS("奏响新时代中国华彩月章"); // 播报文本
        // BKRCspeak_TTS_Num(2023);
    }
}

/*----------------------------------------------------------------------
 * 函数名称：voice_thread_entry
 * 功    能：语音识别子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：使用标签文字显示识别到的内容
 *--------------------------------------------------------------------*/
static void voice_thread_entry(void *arg)
{
    unsigned char tips = 0;

    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备文字标签，放在屏幕上部，并将标签样式应用之上
    voice_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(voice_label, 250);
    lv_obj_align(voice_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 100);
    lv_obj_add_style(voice_label, LV_LABEL_PART_MAIN, &style_label);
    lv_label_set_align(voice_label, LV_LABEL_ALIGN_LEFT);

    // 创建播放按钮，放在文字标签上方，用来播报指定文本
    spk_btn = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_align(spk_btn, voice_label, LV_ALIGN_OUT_TOP_LEFT, 0, -20);
    lv_obj_set_event_cb(spk_btn, spk_btn_event_cb);

    // 按钮上的文字通过标签来呈现
    spk_label = lv_label_create(spk_btn, NULL);
    lv_label_set_text(spk_label, ""
                                 "\xE6\x92\xAD" /*播*/ ""
                                 "\xE6\x8A\xA5" /*报*/
                                 ""
                                 "\xE6\x96\x87" /*文*/ ""
                                 "\xE6\x9C\xAC" /*本*/ "");
    lv_btn_set_fit2(spk_btn, LV_FIT_NONE, LV_FIT_TIGHT);

    while (1)
    {
        UART4_Test();             // 串口4获取数据
        tips = BKRC_VoiceDrive(); // 获取识别到的词条编号
        switch (tips)             // 根据识别到的编号填充显示内容
        {
        case 0x11:
            lv_label_set_text(voice_label,
                              ""
                              "\xE4\xba\xba" /*人*/ ""
                              "\xE6\x89\x8d" /*才*/
                              ""
                              "\xE6\x94\xB9" /*改*/ ""
                              "\xE5\x8F\x98" /*变*/
                              ""
                              "\xE4\xb8\x96" /*世*/ ""
                              "\xE7\x95\x8c" /*界*/ "");
            break;

        case 0x10:
            lv_label_set_text(voice_label,
                              ""
                              "\xE6\x8A\x80" /*技*/ ""
                              "\xE8\x83\xBD" /*能*/
                              ""
                              "\xE6\x88\x90" /*成*/ ""
                              "\xE5\xB0\xB1" /*就*/
                              ""
                              "\xE4\xba\xba" /*人*/ ""
                              "\xE7\x94\x9f" /*生*/ "");
            break;

        case 0x08:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xAE\x9E" /*实*/ ""
                              "\xE8\xB7\xB5" /*践*/
                              ""
                              "\xE9\x94\xBB" /*锻*/ ""
                              "\xE7\x82\xBC" /*炼*/
                              ""
                              "\xE8\x83\xBD" /*能*/ ""
                              "\xE5\x8A\x9B" /*力*/ "");
            break;

        case 0x09:
            lv_label_set_text(voice_label,
                              ""
                              "\xE6\xAF\x94" /*比*/ ""
                              "\xE8\xB5\x9B" /*赛*/
                              ""
                              "\xE5\xBD\xB0" /*彰*/ ""
                              "\xE6\x98\xBE" /*显*/
                              ""
                              "\xE6\x89\x8D" /*才*/ ""
                              "\xE6\x99\xBA" /*智*/ "");
            break;

        case 0x20:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xa5\xbd" /*好*/ ""
                              "\xE7\x9a\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*，*/ ""
                              "\xE7\x81\xAF" /*灯*/
                              ""
                              "\xE5\x85\x89" /*光*/ ""
                              "\xE5\xB7\xB2" /*已*/
                              ""
                              "\xE5\xbc\x80" /*开*/ ""
                              "\xE5\x90\xaf" /*启*/ "");
            if (!blink_started)
            {
                blink_thread_init();
                blink_started = RT_TRUE;
            }
            break;

        case 0x21:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xa5\xbd" /*好*/ ""
                              "\xE7\x9a\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*，*/ ""
                              "\xE7\x81\xAF" /*灯*/
                              ""
                              "\xE5\x85\x89" /*光*/ ""
                              "\xE5\xB7\xB2" /*已*/
                              ""
                              "\xE5\x85\xB3" /*关*/ ""
                              "\xE9\x97\xAD" /*闭*/ "");
            if (blink_started)
            {
                RGB_OFF;
                rt_thread_delete(blink_thread);
                blink_started = RT_FALSE;
            }
            break;

        case 0x22:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xA5\xBD" /*好*/ ""
                              "\xE7\x9A\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*, */ ""
                              "\xE8\x9C\x82" /*蜂*/
                              ""
                              "\xE9\xB8\xA3" /*好*/ ""
                              "\xE5\x99\xA8" /*的*/
                              ""
                              "\xE5\xB7\xB2" /*好*/ ""
                              "\xE6\x89\x93" /*的*/
                              ""
                              "\xE5\xBC\x80" /*好*/ "");
            if (!buzzer_started)
            {
                buzz_flag = BUZZ_TICK;
                buzzer_timer_init();
                buzzer_started = RT_TRUE;
            }
            break;

        case 0x23:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xA5\xBD" /*好*/ ""
                              "\xE7\x9A\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*, */ ""
                              "\xE8\x9C\x82" /*蜂*/
                              ""
                              "\xE9\xB8\xA3" /*好*/ ""
                              "\xE5\x99\xA8" /*的*/
                              ""
                              "\xE5\xB7\xB2" /*好*/ ""
                              "\xE5\x85\xB3" /*的*/
                              ""
                              "\xE9\x97\xAD" /*好*/ "");
            if (buzzer_started)
            {
                buzz_flag = BUZZ_NONE;
                rt_timer_delete(buzzer_timer);
                BUZZER_OFF;
                buzzer_started = RT_FALSE;
            }
            break;

        case 0x24:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xA5\xBD" /*好*/ ""
                              "\xE7\x9A\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*, */ ""
                              "\xE6\xAD\xA3" /*蜂*/
                              ""
                              "\xE5\x9C\xA8" /*好*/ ""
                              "\xE6\xB1\x82" /*的*/
                              ""
                              "\xE6\x95\x91" /*好*/ "");
            if (!buzzer_started)
            {
                buzz_flag = BUZZ_HELP;
                buzzer_timer_init();
                buzzer_started = RT_TRUE;
            }
            break;

        case 0x25:
            lv_label_set_text(voice_label,
                              ""
                              "\xE5\xA5\xBD" /*好*/ ""
                              "\xE7\x9A\x84" /*的*/
                              ""
                              "\xEF\xBC\x8C" /*, */ ""
                              "\xE5\xB7\xB2" /*蜂*/
                              ""
                              "\xE5\x81\x9C" /*好*/ ""
                              "\xE6\xAD\xA2" /*的*/
                              ""
                              "\xE6\xB1\x82" /*好*/ ""
                              "\xE6\x95\x91" /*好*/ "");
            if (buzzer_started)
            {
                buzz_flag = BUZZ_NONE;
                rt_timer_delete(buzzer_timer);
                BUZZER_OFF;
                buzzer_started = RT_FALSE;
            }
            break;

        default:
            break;
        }

        btn_clicked_flag = RT_FALSE; // 按钮点击标志清0
        rt_thread_mdelay(2000);      // 定时挂起线程
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：voice_thread_init
 * 功    能：语音识别子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int voice_thread_init(void)
{
    // 创建子线程，优先级比任务5线程低
    voice_thread = rt_thread_create("voice_thread",
                                    voice_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 2,
                                    THREAD_PRIORITY - 1,
                                    THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (voice_thread != RT_NULL)
    {
        rt_thread_startup(voice_thread);
        rt_kprintf("voice_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create voice_thread failed.\n");
        return -1;
    }
}

//---------------------------------------------------------------------
// 任务5子线程（二级）控制块
//---------------------------------------------------------------------
rt_thread_t task5_thread = RT_NULL;

/*---------------------------------------------------------------------
 * 函数名称：task5_thread_entry
 * 功    能：任务5子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务5下的各子任务（线程）
 *-------------------------------------------------------------------*/
static void task5_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK5) // 主按钮为任务5
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1: // 子任务5.1按钮
                if (!voice_started)
                {
                    voice_thread_init();     // 启动语音识别子线程
                    voice_started = RT_TRUE; // 语音识别子线程标志置位
                }
                break;

            case SUB_TASK_EXIT:    // 任务5退出按钮
                if (voice_started) // 子任务5.1启动标志为真则退出
                {
                    lv_obj_del(spk_label);
                    lv_obj_del(spk_btn);
                    lv_obj_del(voice_label);
                    rt_thread_delete(voice_thread);
                    voice_started = RT_FALSE;
                }
                if (blink_started)
                {
                    RGB_OFF;
                    rt_thread_delete(blink_thread);
                    blink_started = RT_FALSE;
                }
                if (buzzer_started)
                {
                    BUZZER_OFF;
                    rt_timer_delete(buzzer_timer);
                    buzzer_started = RT_FALSE;
                }
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task5_thread_init
 * 功    能：任务5子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task5_thread_init(void)
{
    // 创建任务5子线程，栈空间加大点，为后续图形显示的子任务预留
    task5_thread = rt_thread_create("task5_thread",
                                    task5_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务5子线程
    if (task5_thread != RT_NULL)
    {
        rt_thread_startup(task5_thread);
        rt_kprintf("task5_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task5_thread failed.\n");
        return -1;
    }
}
