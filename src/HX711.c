#include "HX711.h"
#include "stdint.h"

unsigned long HX711_Buffer = 0;
unsigned long Weight_Maopi = 0; 
long  Weight_object = 0;
int usart_weight = 0;
float GapValue = 140;	   //传感器曲率   当发现测试出来的重量偏大时，增加该数值。
//如果测试出来的重量偏小时，减小改数值。该值可以为小数，例如 429.5 等。

/****************************************************************
功  能：HX711端口初始化
参  数：无
返回值：无
****************************************************************/
void HX711_Init(void)
{
	
	gpio_enable(51,DIR_IN);
	gpio_enable(50,DIR_OUT);

}

/****************************************************************
功  能：读取HX711的值
参  数：无
返回值：
****************************************************************/
unsigned long HX711_Read(void)	//增益128
{
	unsigned long count; 
	uint8_t i; 
	
  	HX711_SCK(0); 
  	count = 0; 
  	while(HX711_DOUT); 
	
  	for(i=0; i<24; i++)
	{ 
	  	HX711_SCK(1); 
	  	count = count << 1; 
		HX711_SCK(0); 
	  	if(HX711_DOUT)
			count++; 
	} 
 	HX711_SCK(1); 
    count = count ^ 0x800000;  //第25个脉冲下降沿来时，转换数据
	HX711_SCK(0);  
	return(count);
}

/****************************************************************
功  能：获取毛皮重量
参  数：无
返回值：无
****************************************************************/
void Get_Maopi(void)
{
	Weight_Maopi = HX711_Read();	
} 

/****************************************************************
功  能：称重
参  数：无
返回值：无
****************************************************************/

long Get_Weight(void)
{
    char str[40];
	uint8_t temp[6] = "0000\0";

	HX711_Buffer = HX711_Read();

	Weight_object = HX711_Buffer;
	Weight_object = Weight_object - Weight_Maopi;	   		// 获取实物的AD采样数值。

	if(Weight_object> 0)
	{
		Weight_object= ((float)Weight_object / GapValue); 	// 计算实物的实际重量	
	}
	else
	{
		Weight_object = 0;
	}
	
	temp[0] = Weight_object /10000 %10 + 0x30;
	temp[1] = Weight_object /1000 %10 + 0x30;
	temp[2] = Weight_object /100 %10 + 0x30;
	temp[3] = Weight_object /10 %10 + 0x30;
	usart_weight = (int)(Weight_object/10);
   return Weight_object;
}



