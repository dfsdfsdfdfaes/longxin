//----------------------------------------------------------------
// 必要的头文件
//----------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "gui_layout.h"
#include "ls1x_pwm.h"
#include "drv_pwm.h"
#include "buzzer.h"
#include "task7.h"
#include "calculate.h"

#define PASSWD_LEN 4            //  密码长度
#define MAX_ATTEMPTS 3          //  试错次数
//----------------------------------------------------------------------
// 全局变量
//----------------------------------------------------------------------
static lv_obj_t *play_btnm;                   // 电子琴按钮矩阵
PlayBtnNum play_btn_num = MUSIC_NONE;         // 电子琴按钮索引，初值为无乐曲
static rt_bool_t btn_clicked_flag = RT_FALSE; // 按钮点击标志

static lv_obj_t *calc_btnm;    // 计算器按钮矩阵
static lv_obj_t *calc_textbox; // 计算器上方的文本框

static lv_obj_t *passwd_textbox;    //  密码框
static lv_obj_t *passwd_btnm;       //  密码按钮矩阵
static lv_obj_t *set_btn;           //  设置密码按钮
static lv_obj_t *set_label;         //  设置按钮上的文本标签
static lv_obj_t *unlock_btn;        //  解锁按钮
static lv_obj_t *unlock_label;      //  解锁按钮上的文本标签
static char correct_passwd[PASSWD_LEN + 1] = "1234";    //  预设的正确密码
static char entered_passwd[PASSWD_LEN + 1] = {0};       //  录入的密码，初始为无关内容
static rt_uint8_t attempt_cnt = 0;

typedef enum {
    IDLE,
    SETTING,
    UNLOCKING,
    TRANSITION,
    LOCKED
}LockState;
LockState lock_state = IDLE;

//----------------------------------------------------------------
// 计算器子线程控制块和启动标志
//----------------------------------------------------------------
static rt_thread_t calc_thread = RT_NULL;
static rt_bool_t calc_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：calc_thread_entry
 * 功    能：计算器线程的入口函数
 * 入口参数：按照线程入口函数的参数格式
 * 返 回 值：无
 * 说明描述：生成计算器UI界面，定期把按钮点击标志清0
 *---------------------------------------------------------------*/
static void calc_thread_entry(void *arg)
{
    calc_gui(); // 计算器UI界面

    while (1)
    {
        btn_clicked_flag = RT_FALSE; // 每隔100ms按钮点击标志清0
        rt_thread_mdelay(100);
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：calc_thread_init
 * 功    能：计算器子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int calc_thread_init(void)
{
    // 创建子线程，优先级比主线程低
    calc_thread = rt_thread_create("calc_thread",
                                   calc_thread_entry,
                                   RT_NULL,
                                   THREAD_STACK_SIZE * 4,
                                   THREAD_PRIORITY - 1,
                                   THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (calc_thread != RT_NULL)
    {
        rt_thread_startup(calc_thread);
        rt_kprintf("calc_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create calc_thread failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------
 * 函数名称：calc_btnm_event_cb
 * 功    能：单击计算器按钮执行相关操作
 * 入口参数：按照回调函数的参数格式
 * 返 回 值：无
 * 说明描述：1. 如果是=，就调用逆波兰处理函数计算 = 之前的表达式
 *           2. 如果是1~9 + - * /，就加入文本框中
 *           3. 计算结果需要转换成字符串再显示
 *           4. 借用电子琴音符弹奏的功能，实现按钮嘀一声的效果
 *---------------------------------------------------------------*/
static void calc_btnm_event_cb(lv_obj_t *obj, lv_event_t event)
{
    static double calc_result = 0; // 计算结果
    static char textbuf[MAX_SIZE]; // 字符串缓存

    // 如果按钮按下且点击标志为0，这样可避免连触
    if (event == LV_EVENT_CLICKED && !btn_clicked_flag)
    {
        // 获取按钮对应的字符
        const char *txt = lv_btnmatrix_get_active_btn_text(obj);

        // 如果按下的是=，就计算 = 之前的表达式
        if (!strcmp(txt, "="))
        {
            // 得到文本框里的字符串并进行计算处理
            sprintf(textbuf, "%s", lv_textarea_get_text(calc_textbox));
            calc_result = calculate(textbuf);

            // 无小数则取整数，有小数则结果也呈现小数
            if ((calc_result - (int)calc_result) == 0)
                sprintf(textbuf, "%d", (int)calc_result);
            else
                sprintf(textbuf, "%lf", calc_result);

            // 文本框显示计算结果
            lv_textarea_set_text(calc_textbox, textbuf);
        }
        // 如果按下的是清除，就清0清空
        else if (!strcmp(txt, ""
                              "\xE6\xB8\x85" /*清*/ ""
                              "\xE9\x99\xA4" /*除*/ ""))
        {
            calc_result = 0;
            lv_textarea_set_text(calc_textbox, "");
        }
        // 如果是1~9 +-*/，就加入文本框中
        else
        {
            lv_textarea_add_text(calc_textbox, txt);
        }

        // 借用电子琴音符弹奏的功能，实现按钮嘀一声的效果
        play_btn_num = 0;
        play_flag = PLAY_NOTE;
        BuzzerPWM_Init();
        rt_pwm_control(&rt_ls1x_pwm0, IOCTL_PWM_START, (void *)&buzzer_pwm);

        // 按钮点击标志置位
        btn_clicked_flag = RT_TRUE;
    }
}

/*----------------------------------------------------------------
 * 函数名称：calc_gui
 * 功    能：计算器的UI界面
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：一个文本框，一个按钮矩阵
 *---------------------------------------------------------------*/
void calc_gui(void)
{
    // 计算器上方文本框的创建、尺寸、位置、文字对齐和初始文本
    calc_textbox = lv_textarea_create(lv_scr_act(), NULL);
    lv_obj_align(calc_textbox, NULL, LV_ALIGN_IN_TOP_RIGHT, -180, 20);
    lv_obj_set_size(calc_textbox, 400, 40);
    lv_textarea_set_text_align(calc_textbox, LV_LABEL_ALIGN_LEFT);
    lv_textarea_set_text(calc_textbox, "");

    // 计算器按钮矩阵的创建、布局、尺寸、位置、回调和按钮样式
    calc_btnm = lv_btnmatrix_create(lv_scr_act(), NULL);
    lv_btnmatrix_set_map(calc_btnm, calc_map);
    lv_obj_align(calc_btnm, calc_textbox, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 20);
    lv_obj_set_size(calc_btnm, 400, 380);
    lv_obj_set_event_cb(calc_btnm, calc_btnm_event_cb);
    lv_obj_add_style(calc_btnm, LV_BTNMATRIX_PART_BTN, &style_btnm);
}
/*----------------------------------------------------------------------
 * 函数名称：play_btnm_event_cb
 * 功    能：电子琴按钮事件回调函数
 * 入口参数：按照事件回调函数格式
 * 返 回 值：无
 * 说明描述：设为被按下的状态，并获取按钮索引编号
 *--------------------------------------------------------------------*/
static void play_btnm_event_cb(lv_obj_t *obj, lv_event_t event)
{
    // 如果按钮按下且点击标志为0，这样可避免连触
    if (event == LV_EVENT_CLICKED && !btn_clicked_flag)
    {
        // 按钮点击标志置位
        btn_clicked_flag = RT_TRUE;

        // 重置所有按钮样式为默认状态
        lv_btnmatrix_clear_btn_ctrl_all(obj, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 获取点击的按钮索引
        play_btn_num = lv_btnmatrix_get_active_btn(obj);
        // 设置被点击的按钮为按下状态
        lv_btnmatrix_set_btn_ctrl(obj, play_btn_num, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 设置被点击的按钮样式
        lv_obj_add_style(obj, LV_BTNMATRIX_PART_BTN, &style_btnm);

        if (play_btn_num >= NOTE_1 && play_btn_num <= NOTE_7)
        {
            play_flag = PLAY_NOTE;
            BuzzerPWM_Init();
            rt_pwm_control(&rt_ls1x_pwm0, IOCTL_PWM_START, (void *)&buzzer_pwm);
        }
        else if (play_btn_num == MUSIC_1)
        {
            play_flag = PLAY_MUSIC1;
            BuzzerPWM_Init();
            rt_pwm_control(&rt_ls1x_pwm0, IOCTL_PWM_START, (void *)&buzzer_pwm);
        }
        else if (play_btn_num == MUSIC_2)
        {
            play_flag = PLAY_MUSIC2;
            BuzzerPWM_Init();
            rt_pwm_control(&rt_ls1x_pwm0, IOCTL_PWM_START, (void *)&buzzer_pwm);
        }
    }
}

/*----------------------------------------------------------------------
 * 函数名称：play_btnm_gui
 * 功    能：创建并布局电子琴按钮
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：创建按钮矩阵并配置
 *--------------------------------------------------------------------*/
void play_btnm_gui(void)
{
    // 创建音符按钮矩阵
    play_btnm = lv_btnmatrix_create(lv_scr_act(), NULL);
    // 布局按钮
    lv_btnmatrix_set_map(play_btnm, play_btnm_map);
    // 设置大小
    lv_obj_set_size(play_btnm, 420, 240);
    // 屏幕右上位置
    lv_obj_align(play_btnm, NULL, LV_ALIGN_IN_TOP_RIGHT, -40, 40);
    // 注册按钮事件回调函数
    lv_obj_set_event_cb(play_btnm, play_btnm_event_cb);
    // 将样式应用于按钮
    lv_obj_add_style(play_btnm, LV_BTNMATRIX_PART_BTN, &style_btnm);
}

//---------------------------------------------------------------------
// 生成电子琴按钮子线程（子任务7.1）控制块、启动标志和入口函数
//---------------------------------------------------------------------
static rt_thread_t play_btnm_thread = RT_NULL;
static rt_bool_t play_btnm_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：play_btnm_thread_entry
 * 功    能：生成电子琴按钮矩阵
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：需要用挂起维持该线程
 *--------------------------------------------------------------------*/
static void play_btnm_thread_entry(void *arg)
{
    play_btnm_gui(); // 生成乐曲按钮矩阵

    while (1)
    {
        btn_clicked_flag = RT_FALSE; // 按钮点击标志清0
        rt_thread_mdelay(100);       // 适当的挂起时间
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：play_btnm_thread_init
 * 功    能：电子琴按钮子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int play_btnm_thread_init(void)
{
    // 创建子线程，优先级比主线程低
    play_btnm_thread = rt_thread_create("play_btnm_thread",
                                        play_btnm_thread_entry,
                                        RT_NULL,
                                        THREAD_STACK_SIZE * 2,
                                        THREAD_PRIORITY - 1,
                                        THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (play_btnm_thread != RT_NULL)
    {
        rt_thread_startup(play_btnm_thread);
        rt_kprintf("play_btnm_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create play_btnm_thread failed.\n");
        return -1;
    }
}


/*----------------------------------------------------------------
 * 代码清单3：update_display函数
 * 功    能：更新显示已录入的密码
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：呈现 * *_ _ 的显示效果
 *---------------------------------------------------------------*/
void update_display(void)
{
    char display_passwd[PASSWD_LEN + 1];    //存放显示内容的数组
    rt_uint8_t i;   //循环控制下标

    //有录入的地方用 * 显示，未录用的地方用 _ 显示
    for(i=0; i<PASSWD_LEN; i++)
        display_passwd[i] = entered_passwd[i] ? '*' : '_';

    display_passwd[PASSWD_LEN] = '\0';  //添加字符串结束标志

    //当前内容显示在文本框里
    lv_textarea_set_text(passwd_textbox, display_passwd);
}

/*----------------------------------------------------------------
 * 代码清单2：passwd_event_cb函数
 * 功    能：密码锁主键盘按钮的回调函数
 * 入口参数：按照按钮事件回调函数的参数格式
 * 返 回 值：无
 * 说明描述：根据当前状态与按键来控制状态的转换
 *---------------------------------------------------------------*/
void passwd_event_cb(lv_obj_t *obj, lv_event_t event)
{
    rt_uint8_t i = 0;   //循环控制下标

    //如果按钮按下且点击标志为0，这样可避免连触
    if(event == LV_EVENT_VALUE_CHANGED && !btn_clicked_flag)
    {
        //按钮点击标志置位
        btn_clicked_flag = RT_TRUE;

        //获取点击按钮上的内容（字符串）
        const char *text = lv_btnmatrix_get_active_btn_text(obj);

        //根据按下的钮，结合密码锁当前状态，决定密码锁状态切换
        if(text)
        {
            //如果在空闲态按下数字键：进入解锁中状态，存入按下的第一个数字，更新显示
            if(lock_state == IDLE && text[0] != '#' && text[0] != '*')
            {
                lock_state = UNLOCKING;
                entered_passwd[0] = text[0];
                update_display();
            }

            //如果在设置态按下数字键：录入1位存入1位，更新显示
            else if(lock_state == SETTING && text[0] != '#' && text[0] != '*')
            {
                for(i=0; i<PASSWD_LEN; i++)
                {
                    if(entered_passwd[i] == 0)
                    {
                        entered_passwd[i] = text[0];
                        update_display();
                        break;
                    }
                }
            }

            //如果在解锁中状态按下数字键：录入1位存入1位，更新显示
            else if(lock_state == UNLOCKING && text[0] != '#' && text[0] != '*')
            {
                for(i=0; i<PASSWD_LEN; i++)
                {
                    if(entered_passwd[i] == 0)
                    {
                        entered_passwd[i] = text[0];
                        update_display();
                        break;
                    }
                }
            }

            //如果在设置态按下 # 键：切换到过渡态，存入新密码，清空已录入的密码，显示设置成功
            else if(lock_state == SETTING && text[0] == '#')
            {
                lock_state = TRANSITION;
                memcpy(correct_passwd, entered_passwd, PASSWD_LEN);
                memset(entered_passwd, 0, PASSWD_LEN);
                lv_textarea_set_text(passwd_textbox,
                                     """\xE8\xAE\xBE"/*设*/"""\xE7\xBD\xAE"/*置*/
                                     """\xE6\x88\x90"/*成*/"""\xE5\x8A\x9F"/*功*/"");
            }

            //如果在过渡态按下 * 键：关灯，切换到空闲态，清空显示
            else if(lock_state == TRANSITION && text[0] == '*')
            {
                lock_state = IDLE;
                lv_textarea_set_text(passwd_textbox, "");
            }

            //如果在锁住态按下 * 键：关闭报警、关灯、关蜂鸣器，切换到空闲态，清空显示
            else if(lock_state == LOCKED && text[0] == '*')
            {
                lock_state = IDLE;
                lv_textarea_set_text(passwd_textbox, "");
            }
        }
    }
}

/*----------------------------------------------------------------
 * 代码清单3：unlock_event_cb函数
 * 功    能：按下解锁按钮的回调函数
 * 入口参数：按照按钮事件回调函数的参数格式
 * 返 回 值：无
 * 说明描述：进行密码比对，根据错误次数控制结果显示和状态转换
 *---------------------------------------------------------------*/
void unlock_event_cb(lv_obj_t *obj, lv_event_t event)
{
    //如果按钮按下且点击标志为0，这样可避免连触
    if(event == LV_EVENT_CLICKED && !btn_clicked_flag)
    {
        //按钮点击标志置位
        btn_clicked_flag = RT_TRUE;

        //如果当前是解锁中的状态就判断密码
        if(lock_state == UNLOCKING)
        {
            //密码正确显示开锁成功，切换到过渡态，清空开锁次数和已录入的密码
            if(strcmp(entered_passwd, correct_passwd) == 0)
            {
                lv_textarea_set_text(passwd_textbox,
                                     """\xE5\xBC\x80"/*开*/"""\xE9\x94\x81"/*锁*/
                                     """\xE6\x88\x90"/*成*/"""\xE5\x8A\x9F"/*功*/"");
                lock_state = TRANSITION;
                attempt_cnt = 0;
                memset(entered_passwd, 0, PASSWD_LEN);
            }

            //密码错误的处理，分少于3次和3次以上分别处理
            else
            {
                attempt_cnt++;    //累加错误次数

                //错误3次以上显示错误已锁，切换到锁住态，清空已录入密码和错误次数
                if(attempt_cnt >= MAX_ATTEMPTS)
                {
                    lv_textarea_set_text(passwd_textbox,
                                         """\xE9\x94\x99"/*错*/"""\xE8\xAF\xAF"/*误*/
                                         """\xE5\xB7\xB2"/*已*/"""\xE9\x94\x81"/*锁*/"");
                    lock_state = LOCKED;
                    memset(entered_passwd, 0, PASSWD_LEN);
                    attempt_cnt = 0;
                }

                //错误3次以内显示密码错误，切换到过渡态，清空已录入的密码
                else
                {
                    lv_textarea_set_text(passwd_textbox,
                                         """\xE5\xAF\x86"/*密*/"""\xE7\xA0\x81"/*码*/
                                         """\xE9\x94\x99"/*错*/"""\xE8\xAF\xAF"/*误*/"");
                    memset(entered_passwd, 0, PASSWD_LEN);
                    lock_state = TRANSITION;
                }
            }
        }
    }
}

/*----------------------------------------------------------------
 * 代码清单4：set_event_cb函数
 * 功    能：按下设置按钮的回调函数
 * 入口参数：按照按钮事件回调函数的参数格式
 * 返 回 值：无
 * 说明描述：切换至设置态，清空已录入密码，更新显示
 *---------------------------------------------------------------*/
void set_event_cb(lv_obj_t *obj, lv_event_t event)
{
    //如果按钮按下且点击标志为0，这样可避免连触
    if(event == LV_EVENT_CLICKED && !btn_clicked_flag)
    {
        //按钮点击标志置位
        btn_clicked_flag = RT_TRUE;

        //密码锁进入设置状态
        lock_state = SETTING;

        //清空前面已录入的密码
        memset(entered_passwd, 0, PASSWD_LEN);

        //此时更新显示成 _ _ _ _
        update_display();
    }
}
/*----------------------------------------------------------------
 * 代码清单5：lock_gui函数
 * 功    能：密码锁的UI界面
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：一个文本框、一个按钮矩阵、一个设置密码按钮、一个解锁按钮
 *---------------------------------------------------------------*/
void lock_gui(void)
{
    //创建一个文本框，用来显示已录入的密码，初始内容为空
    passwd_textbox = lv_textarea_create(lv_scr_act(), NULL);
    lv_obj_align(passwd_textbox, NULL, LV_ALIGN_IN_TOP_RIGHT, -120, 20);
    lv_obj_set_size(passwd_textbox, 300, 50);
    lv_textarea_set_text_align(passwd_textbox, LV_LABEL_ALIGN_CENTER);
    lv_textarea_set_text(passwd_textbox, "");

    //创建一个数字按钮矩阵，用来录入密码，位于密码框下面
    passwd_btnm = lv_btnmatrix_create(lv_scr_act(), NULL);
    lv_btnmatrix_set_map(passwd_btnm, passwd_map);
    lv_obj_align(passwd_btnm, passwd_textbox, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 20);
    lv_obj_set_size(passwd_btnm, 300, 300);
    lv_obj_set_event_cb(passwd_btnm, passwd_event_cb);
	lv_obj_add_style(passwd_btnm, LV_BTNMATRIX_PART_BTN, &style_btnm);

    //创建一个按钮，用来设置密码
    set_btn = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_align(set_btn, passwd_btnm, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 20);
    lv_obj_set_event_cb(set_btn, set_event_cb);
    set_label = lv_label_create(set_btn, NULL);
    lv_label_set_text(set_label, """\xE8\xAE\xBE"/*设*/"""\xE7\xBD\xAE"/*置*/
                                 """\xE5\xAF\x86"/*密*/"""\xE7\xA0\x81"/*码*/"");

    //创建一个按钮，用来解锁
    unlock_btn = lv_btn_create(lv_scr_act(), NULL);
    lv_obj_align(unlock_btn, passwd_btnm, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, 20);
    lv_obj_set_event_cb(unlock_btn, unlock_event_cb);
    unlock_label = lv_label_create(unlock_btn, NULL);
    lv_label_set_text(unlock_label, """\xE8\xA7\xA3"/*解*/"""\xE9\x94\x81"/*锁*/"");
}


//----------------------------------------------------------------
// 密码锁子线程（子任务7.3）控制块和启动标志
//----------------------------------------------------------------
static rt_thread_t lock_thread = RT_NULL;
static rt_bool_t lock_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：lock_thread_entry
 * 功    能：密码锁线程入口函数
 * 入口参数：按照线程入口函数的参数格式
 * 返 回 值：无
 * 说明描述：启动密码锁UI界面，定期清除按钮点击标志
 *---------------------------------------------------------------*/
static void lock_thread_entry(void *arg)
{
    lock_gui();     //密码锁界面

    while(1)
    {
        btn_clicked_flag = RT_FALSE;    //每隔100ms按钮点击标志清0
        rt_thread_mdelay(100);
    }
}

/*----------------------------------------------------------------
 * 函数名称：lock_thread_init
 * 功    能：密码锁子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int lock_thread_init(void)
{
    //创建子线程，优先级比主线程低
    lock_thread = rt_thread_create("lock_thread",
                                    lock_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 1,
                                    THREAD_TIMESLICE);

    //创建成功就启动子线程
    if(lock_thread != RT_NULL)
    {
        rt_thread_startup(lock_thread);
        rt_kprintf("lock_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create lock_thread failed.\n");
        return -1;
    }
}

/*-----------------------------------------------------------------------
 * 代码清单7：lock_task_end函数
 * 功    能：结束密码锁UI、子线程、清空录入的密码、恢复初始密码
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：删除对象的时候注意次序，如果对象B是基于对象A来创建或布局的，
 *           应对先删除对象B，再删除对象A
 *---------------------------------------------------------------------*/
void lock_task_end(void)
{
    lv_obj_del(passwd_btnm);        //按钮是基于文本框来布局的，要先删
    lv_obj_del(passwd_textbox);     //后删文本框
    lv_obj_del(set_label);          //标签是基于按钮来创建的，要先删
    lv_obj_del(set_btn);            //后删按钮
    lv_obj_del(unlock_label);       //与上同理
    lv_obj_del(unlock_btn);

    rt_thread_delete(lock_thread);  //结束密码锁子线程

    memset(entered_passwd, 0, PASSWD_LEN+1);      //清空已录入密码
    memcpy(correct_passwd, "1234", PASSWD_LEN+1);   //恢复初始密码
}

//---------------------------------------------------------------------
// 任务7子线程（二级）控制块
//---------------------------------------------------------------------
rt_thread_t task7_thread = RT_NULL;

/*---------------------------------------------------------------------
 * 函数名称：task7_thread_entry
 * 功    能：任务7子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务7下的各子任务（线程）
 *-------------------------------------------------------------------*/
static void task7_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK7) // 主按钮为任务7
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1: // 子任务7.1按钮
                if (calc_started)
                {
                    lv_obj_del(calc_btnm);
                    lv_obj_del(calc_textbox);
                    rt_thread_delete(calc_thread);
                    calc_started = RT_FALSE;
                }
                if(lock_started)            //  如果密码锁在运行则结束它
                {
                    lock_task_end();
                    lock_started = RT_FALSE;
                    lock_state = IDLE;
                }
                if (!play_btnm_started)
                {
                    play_btnm_thread_init();     // 启动电子琴按钮的子线程
                    play_btnm_started = RT_TRUE; // 电子琴按钮子线程标志置位
                }
                break;

            case SUB_TASK_2:
                if (play_btnm_started)
                {
                    lv_obj_del(play_btnm);
                    rt_thread_delete(play_btnm_thread);
                    play_btnm_started = RT_FALSE;
                }
                if(lock_started)            //  如果密码锁在运行则结束它
                {
                    lock_task_end();
                    lock_started = RT_FALSE;
                    lock_state = IDLE;
                }
                if (!calc_started)
                {
                    calc_thread_init();
                    calc_started = RT_TRUE;
                }
                break;
                
            case SUB_TASK_3:
                if (play_btnm_started)
                {
                    lv_obj_del(play_btnm);
                    rt_thread_delete(play_btnm_thread);
                    play_btnm_started = RT_FALSE;
                }
                if (calc_started)
                {
                    lv_obj_del(calc_btnm);
                    lv_obj_del(calc_textbox);
                    rt_thread_delete(calc_thread);
                    calc_started = RT_FALSE;
                }
                if(!lock_started)            //  如果密码锁在运行则结束它
                {
                    lock_thread_init();
                    lock_started = RT_TRUE;
                }
                break;

            case SUB_TASK_EXIT:        // 退出按钮
                if (play_btnm_started) // 如果电子琴在运行则结束它
                {
                    lv_obj_del(play_btnm);
                    rt_thread_delete(play_btnm_thread);
                    play_btnm_started = RT_FALSE;
                }
                if(lock_started)            //  如果密码锁在运行则结束它
                {
                    lock_task_end();
                    lock_started = RT_FALSE;
                    lock_state = IDLE;
                }
                if (calc_started)
                {
                    lv_obj_del(calc_btnm);
                    lv_obj_del(calc_textbox);
                    rt_thread_delete(calc_thread);
                    calc_started = RT_FALSE;
                }
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task7_thread_init
 * 功    能：任务7子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task7_thread_init(void)
{
    // 创建任务7子线程，栈空间加大点，为后续图形显示的子任务预留
    task7_thread = rt_thread_create("task7_thread",
                                    task7_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务7子线程
    if (task7_thread != RT_NULL)
    {
        rt_thread_startup(task7_thread);
        rt_kprintf("task7_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task7_thread failed.\n");
        return -1;
    }
}
