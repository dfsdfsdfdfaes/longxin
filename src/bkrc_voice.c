/**
 * @file 语音模块驱动代码
 * @author 张霖鑫
 * @brief
 * @version 0.1
 * @date 2023-11-5
 *
 * @copyright Copyright (c) 2024
 *
 */
//-----------------------------------------------------------
// 必要的头文件
//-----------------------------------------------------------
#include "bkrc_voice.h"
#include "ls1b.h"
#include "ls1b_gpio.h"
#include "ns16550.h"
#include "stdio.h"
#include "string.h"

//-----------------------------------------------------------
// 全局变量
//-----------------------------------------------------------
char buff[256];                // 接收的内容缓冲区
unsigned char uart4_flag = 0;  // 串口4接收数据的计数标记
unsigned char uart4_RxData[8]; // 串口4接收数据的缓冲区
unsigned char voice_flag = 0;  // 语音模块返回的标志

/*------------------------------------------------------------------
 * 函数名称：UART4_Init
 * 函数功能：串口4初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：UART4_RX:58 --- 数据接收，UART4_TX:59 --- 数据发送
 *           波特率115200
 * ---------------------------------------------------------------*/
void UART4_Init(void)
{
    unsigned int baudrate = 115200;             // 波特率
    ls1x_uart_init(devUART4, (void *)baudrate); // 初始化串口
    ls1x_uart_open(devUART4, NULL);             // 打开串口
}

/*------------------------------------------------------------------
 * 函数名称：UART4_Test
 * 函数功能：串口4获取数据并处理
 * 入口参数：无
 * 返 回 值：无
 * 说    明：把语音模块发来的数据存入缓冲区，并拆分数据帧
 * ---------------------------------------------------------------*/
void UART4_Test(void)
{
    // 串口4接收到有效数据，存入缓冲区
    if (ls1x_uart_read(devUART4, buff, 256, NULL) > 0)
    {
        ls1x_uart_write(devUART4, buff, 8, NULL);
        if (uart4_flag == 0x00)
        {
            if (buff[0] == 0x55) // 自定义数据帧头
            {
                uart4_flag = 0x01;
                uart4_RxData[0] = buff[0]; // 帧头
                uart4_RxData[1] = 0x00;
                uart4_RxData[2] = 0x00;
                uart4_RxData[3] = 0x00;
            }
        }
        else if (uart4_flag == 0x01)
        {
            uart4_flag = 0x02;
            uart4_RxData[1] = buff[0]; // 数据类型
        }
        else if (uart4_flag == 0x02)
        {
            uart4_flag = 0x03;
            uart4_RxData[2] = buff[0]; // 状态标志
        }
        else if (uart4_flag == 0x03)
        {
            uart4_flag = 0x00;
            uart4_RxData[3] = buff[0]; // 数据位
            voice_flag = 0x01;         // 自定义数据帧接收完毕
        }
        else // 其他情况则清空
        {
            uart4_flag = 0x00;
            voice_flag = 0x00;
            uart4_RxData[0] = 0x00;
        }
    }
}

/*-----------------------------------------------------------
 * 函数名称：BKRCspeak_TTS
 * 函数功能：播报字符串（包括中文）
 * 入口参数：*dat --- 字符串
 * 返 回 值：无
 * 说    明：使用样例BKRCspeak_TTS("你好");
 * --------------------------------------------------------*/
void BKRCspeak_TTS(char *dat)
{
    while (*dat)
    {
        if ((unsigned char)*dat < 0x80)
        {
            // 字符在标准ASCII码表范围内（0x00 ~ 0x7F）
            // 将该字符通过串口4发给语音模块，长度1个字节
            ls1x_uart_write(devUART4, dat, 1, NULL);
            dat++;
            delay_ms(10);
        }
        else
        {
            // 字符在超过了标准ASCII码表范围，说明是中文
            // 查看serialTTS.txt文件，可见中文存储占两个字节
            ls1x_uart_write(devUART4, dat, 1, NULL);
            ls1x_uart_write(devUART4, dat + 1, 1, NULL);
            dat += 2;
            delay_ms(10);
        }
    }
}

/*-----------------------------------------------------------
 * 函数名称：BKRCspeak_TTS_Num
 * 函数功能：播报数字
 * 入口参数：num --- 数字
 * 返 回 值：无
 * 说    明：使用样例BKRCspeak_TTS_Num(123);
 * --------------------------------------------------------*/
void BKRCspeak_TTS_Num(uint num)
{
    char str[50];
    sprintf(str, "%d", num); // 把数字格式化成字符串
    BKRCspeak_TTS(str);      // 再按字符串播报
}

/*-----------------------------------------------------------
 * 函数名称：BKRC_VoiceDrive
 * 函数功能：语音识别函数
 * 入口参数：无
 * 返 回 值：烧写词条的编号
 * 说    明：对照asrWordlist.txt文件，词条编号在返回数据帧的
 *           第3个字节，取出该字节即可
 * --------------------------------------------------------*/
unsigned char BKRC_VoiceDrive(void)
{
    unsigned char voice_status = 0; // 用来存放词条编号

    if (buff[0] == 0x55) // 自定义数据帧的首字节
    {
        if (buff[1] == 0x02) // 自定义数据帧的第二个字节
        {
            if (buff[2] >= 0x01 && buff[2] <= 0x60)
            { // 词条编号在预设的范围内就取出
                voice_status = buff[2];
            }
        }

        voice_flag = 0x00; // 完成一次识别，标志清0
    }
    return voice_status; // 返回该词条编号
}
