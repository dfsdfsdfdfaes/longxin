//-------------------------------------------------------------------------
// 必要的头文件
//-------------------------------------------------------------------------
#include "bsp.h"
#include "ls1b.h"
#include "ls1b_gpio.h"
#include "ls1x_pwm.h"
#include "drv_pwm.h"
#include "ultrasonic_ranging_drv.h"

//-------------------------------------------------------------------------
// 全局变量
//-------------------------------------------------------------------------
unsigned int meas_time = 0;                 //超声波测距计时
float distance = 0, last_distance = 0;      //本次和上次测得的距离
pwm_cfg_t pwm2_cfg;                         //定义一个PWM的结构体


/* -----------------------------------------------------------------------
 * 函数名称：PWM2Timer_cb
 * 函数功能：PWM2定时器回调处理函数
 * 入口参数：按定时器回调函数入口参数
 * 返 回 值：无
 * 说    明：测距计时累加
 * -------------------------------------------------------------------- */
static void PWM2Timer_cb(void *pwm, int *stopit)
{
    meas_time++;
}

/* ----------------------------------------------------------------------
 * 函数名称：PWM2Timer_Init
 * 函数功能：使用PWM2实现约10us定时
 * 入口参数：无
 * 返 回 值：无
 * 说    明：参数上虽然是ns，但并不准确，也没找到资料解释参数和时长的关系，
 *           暂时根据官方例程的注释，参数5000约为1us
 * ------------------------------------------------------------------- */
void PWM2Timer_Init(void)
{
    pwm2_cfg.hi_ns = 5000;  //高电平脉宽（ns），定时器模式仅用hi_ns（约1us）
    //pwm2_cfg.lo_ns = 0;   //低电平脉宽（ns），定时器模式没用lo_ns
    pwm2_cfg.mode = PWM_CONTINUE_TIMER; //连续定时器模式
    pwm2_cfg.cb = PWM2Timer_cb;         //定时器回调函数
    pwm2_cfg.isr = NULL;                //工作在定时器模式

    //注册PWM控制器（定时器），暂不启动
    rt_ls1x_pwm_install();
    rt_pwm_control(&rt_ls1x_pwm2, IOCTL_PWM_STOP, (void *)&pwm2_cfg);
}

/* ---------------------------------------------------------------------
 * 函数名称：ECHO_IRQHandler
 * 函数功能：ECHO返回下降沿产生中断
 * 入口参数：无
 * 返 回 值：无
 * 说    明：停止测距计时，计算距离
 * ------------------------------------------------------------------ */
void ECHO_IRQHandler(int IRQn, void *param)
{
    rt_pwm_control(&rt_ls1x_pwm2, IOCTL_PWM_STOP, (void *)&pwm2_cfg);
    distance = meas_time*1.7/10/1.2;		//1.2为定时器补偿
    meas_time = 0;
}

/* -----------------------------------------------------------------------
 * 函数名称：Ultrasonic_Init
 * 函数功能：超声波端口初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：TRIG输出，ECHO输入，下降沿触发中断
 * -------------------------------------------------------------------- */
void Ultrasonic_Init(void)
{
    gpio_enable(TRIG, DIR_OUT);
    gpio_enable(ECHO, DIR_IN);

    //下降沿触发GPIO中断，并使能该引脚中断
    ls1x_install_gpio_isr(ECHO, INT_TRIG_EDGE_DOWN, ECHO_IRQHandler, NULL);
    ls1x_enable_gpio_interrupt(ECHO);

    //发送引脚默认设置为低电平
    gpio_write(TRIG, 0);
}

/* ------------------------------------------------------------------------
 * 函数名称：Ultrasonic_Start
 * 函数功能：开始超声波测距
 * 入口参数：无
 * 返 回 值：无
 * 说    明：产生测量所需脉冲，启动定时器计时
 * --------------------------------------------------------------------- */
void Ultrasonic_Start(void)
{
    unsigned char i;

    //给TRIG连续4个高脉冲启动测距，周期24us，频率近40kHz
    for(i=0; i<4; i++)
    {
        gpio_write(TRIG, 1);
        delay_us(12);
        gpio_write(TRIG, 0);
        delay_us(12);
    }

    //启动定时器，开始测距
    rt_pwm_control(&rt_ls1x_pwm2, IOCTL_PWM_START, (void *)&pwm2_cfg);
}
