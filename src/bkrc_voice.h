//------------------------------------------------------------------
// 代码清单1：bkrc_voice.h文件源码
//------------------------------------------------------------------

#ifndef _BKRC_VOICE_H
#define _BKRC_VOICE_H

//------------------------------------------------------------------
// 函数声明
//------------------------------------------------------------------
void UART4_Init(void);                    // 串口4初始化函数
void UART4_Test(void);                    // 串口4获取数据并处理
void BKRCspeak_TTS(char *dat);            // 播报字符（含中文）函数
void BKRCspeak_TTS_Num(unsigned int num); // 播报数字函数
unsigned char BKRC_VoiceDrive(void);      // 语音识别函数

#endif
