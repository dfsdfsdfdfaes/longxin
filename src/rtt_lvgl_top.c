

//-----------------------------------------------------------
// 必要的库头文件
//-----------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "rtthread.h"

//-----------------------------------------------------------
// LVGL头文件
//-----------------------------------------------------------
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "lv_port_fs.h"

//-----------------------------------------------------------
// BSP头文件
//-----------------------------------------------------------
#include "bsp.h"

//-----------------------------------------------------------
// src头文件
//-----------------------------------------------------------
#include "my_def.h"
#include "gui_layout.h"
#include "rtt_lvgl_top.h"
#include "task1.h"
#include "task2.h"
#include "task3.h"
#include "task4.h"
#include "task5.h"
#include "task6.h"
#include "task7.h"

//-----------------------------------------------------------
// 全局变量
//-----------------------------------------------------------
lv_style_t style_btnm;                   // 按钮矩阵样式
lv_style_t style_label;                  // 文字标签样式
TaskNum task_num = NO_TASK;              // 主按钮索引编号，初值为无关数
static lv_obj_t *mbtnm;                  // 一级按钮矩阵
SubTaskNum sub_task_num = SUB_TASK_NONE; // 二级按钮索引编号，初值为无关数
static lv_obj_t *sbtnm;                  // 二级按钮矩阵
TaskNum taskx_flag = NO_TASK;            // 当前任务标志，初值为无任务
//-----------------------------------------------------------
// 引用的外部函数
//-----------------------------------------------------------
extern void lv_port_disp_init(void);  // in "lv_port_disp.c"
extern void lv_port_indev_init(void); // in "lv_port_indev.c"
extern void lv_port_fs_init(void);    // in "lv_port_fs.c"

/*----------------------------------------------------------------
 * 代码清单6：mbtnm_event_cb函数
 * 功    能：主按钮事件回调函数
 * 入口参数：按照回调函数的参数格式
 * 返 回 值：无
 * 说明描述：修改的部分是前两个case的代码块
 *---------------------------------------------------------------*/
static void mbtnm_event_cb(lv_obj_t *obj, lv_event_t event)
{
    if (event == LV_EVENT_CLICKED)
    {
        // 重置所有按钮样式为默认状态
        lv_btnmatrix_clear_btn_ctrl_all(obj, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 获取点击的按钮索引
        task_num = lv_btnmatrix_get_active_btn(obj);
        // 设置被点击的按钮为按下状态
        lv_btnmatrix_set_btn_ctrl(obj, task_num, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 设置被点击的按钮样式
        lv_obj_add_style(obj, LV_BTNMATRIX_PART_BTN, &style_btnm);

        // 根据主按钮索引号决定二级按钮布局和运行的二级子线程
        switch (task_num)
        {
        case TASK1:                  // 按下的是任务1主按钮
            if (taskx_flag != TASK1) // 如果当前运行的不是任务1
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task1_map); // 布局成任务1二级按钮
                task1_thread_init();                    // 运行任务1子线程
                taskx_flag = TASK1;                     // 置成任务1标志
            }
            break;

        case TASK2:
            if (taskx_flag != TASK2) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task2_map); // 布局成任务2二级按钮
                task2_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK2;                     // 置成任务2标志
            }
            break;

        case TASK3:
            if (taskx_flag != TASK3) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task3_map); // 布局成任务2二级按钮
                task3_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK3;                     // 置成任务2标志
            }
            break;
        case TASK4:
            if (taskx_flag != TASK4) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task4_map); // 布局成任务2二级按钮
                task4_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK4;
            }
            break;
        case TASK5:
            if (taskx_flag != TASK5) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task5_map); // 布局成任务2二级按钮
                task5_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK5;
            }
            break;
        case TASK6:
            if (taskx_flag != TASK6) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK7:
                    rt_thread_delete(task7_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task6_map); // 布局成任务2二级按钮
                task6_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK6;
            }
            break;
        case TASK7:
            if (taskx_flag != TASK7) // 如果当前运行的不是任务2
            {
                switch (taskx_flag) // 其他哪个任务在运行先删掉其线程
                {
                case TASK1:
                    rt_thread_delete(task1_thread);
                    break;
                case TASK2:
                    rt_thread_delete(task2_thread);
                    break;
                case TASK3:
                    rt_thread_delete(task3_thread);
                    break;
                case TASK4:
                    rt_thread_delete(task4_thread);
                    break;
                case TASK5:
                    rt_thread_delete(task5_thread);
                    break;
                case TASK6:
                    rt_thread_delete(task6_thread);
                    break;
                // case TASK8: rt_thread_delete(task8_thread); break;
                default:
                    break;
                }
                lv_btnmatrix_set_map(sbtnm, task7_map); // 布局成任务2二级按钮
                task7_thread_init();                    // 运行任务2子线程
                taskx_flag = TASK7;
            }
            break;
        case TASK8:
            lv_btnmatrix_set_map(sbtnm, task8_map);
            break;
        default:
            break;
        }
    }
}

/*-------------------------------------------------------------------------
 * 函数名称：sbtnm_event_cb
 * 功    能：二级按钮事件回调函数
 * 入口参数：按照回调函数的参数格式
 * 返 回 值：无
 * 说明描述：暂且只变化一下样式，不做其他控制
 *-----------------------------------------------------------------------*/
static void sbtnm_event_cb(lv_obj_t *obj, lv_event_t event)
{
    if (event == LV_EVENT_CLICKED)
    {
        // 重置所有按钮样式为默认状态
        lv_btnmatrix_clear_btn_ctrl_all(obj, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 获取点击的按钮索引
        sub_task_num = lv_btnmatrix_get_active_btn(obj);
        // 设置被点击的按钮为按下状态
        lv_btnmatrix_set_btn_ctrl(obj, sub_task_num, LV_BTNMATRIX_CTRL_CHECK_STATE);
        // 设置被点击的按钮样式
        lv_obj_add_style(obj, LV_BTNMATRIX_PART_BTN, &style_btnm);
    }
}

/*----------------------------------------------------------------
 * 函数名称：top_gui
 * 功    能：主界面的UI设置
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：布局两级按钮矩阵效果
 *---------------------------------------------------------------*/
void top_gui(void)
{
    // 创建一级（主）按钮矩阵
    mbtnm = lv_btnmatrix_create(lv_scr_act(), NULL);
    // 按布局数组设置按钮
    lv_btnmatrix_set_map(mbtnm, main_map);
    // 设置大小尺寸
    lv_obj_set_size(mbtnm, 150, 450);
    // 设置对齐（屏幕内水平靠左，垂直居中）
    lv_obj_align(mbtnm, NULL, LV_ALIGN_IN_LEFT_MID, 0, 0);
    // 注册主按钮事件回调函数
    lv_obj_set_event_cb(mbtnm, mbtnm_event_cb);

    // 新增部分：创建二级按钮矩阵
    sbtnm = lv_btnmatrix_create(lv_scr_act(), NULL);
    // 默认按任务1布局数组设置按钮
    lv_btnmatrix_set_map(sbtnm, task1_map);
    // 设置大小尺寸
    lv_obj_set_size(sbtnm, 150, 300);
    // 设置对齐（主按钮右侧）
    lv_obj_align(sbtnm, mbtnm, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
    // 注册二级按钮事件回调函数
    lv_obj_set_event_cb(sbtnm, sbtnm_event_cb);

    // 初始化按钮矩阵样式
    lv_style_init(&style_btnm);
    // 设置样式按下的背景色
    lv_style_set_bg_color(&style_btnm, LV_STATE_PRESSED, LV_COLOR_GREEN);
    // 设置样式默认和按下状态下的字体颜色
    lv_style_set_text_color(&style_btnm, LV_STATE_DEFAULT, LV_COLOR_WHITE);
    lv_style_set_text_color(&style_btnm, LV_STATE_PRESSED, LV_COLOR_WHITE);
    // 设置样式默认状态下的边框和轮廓
    lv_style_set_border_width(&style_btnm, LV_STATE_DEFAULT, 1);
    lv_style_set_outline_width(&style_btnm, LV_STATE_DEFAULT, 0);
    // 将样式应用于主按钮和二级按钮
    lv_obj_add_style(mbtnm, LV_BTNMATRIX_PART_BTN, &style_btnm);
    lv_obj_add_style(sbtnm, LV_BTNMATRIX_PART_BTN, &style_btnm);
}

//----------------------------------------------------------------------------
// 主界面线程
//----------------------------------------------------------------------------
static rt_thread_t top_gui_thread = RT_NULL;

/*----------------------------------------------------------------
 * 函数名称：top_gui_thread_entry
 * 功    能：生成主界面UI线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：要处理LVGL任务，需要定期在OS任务中调用lv_task_handler()
 *---------------------------------------------------------------*/
static void top_gui_thread_entry(void *arg)
{
    top_gui(); // 调用函数来生成主界面UI

    while (1)
    {
        lv_task_handler();   // 在主线程中定期调用LVGL任务处理器
        rt_thread_mdelay(5); // 计时并不严格，大约5ms以保持系统响应
    }
}

/*----------------------------------------------------------------
 * 函数名称：rtt_lvgl_init
 * 功    能：LVGL相关的初始化，创建主界面线程
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int rtt_lvgl_init(void)
{
    lv_init();            // LVGL初始化
    lv_port_disp_init();  // 显示初始化
    lv_port_indev_init(); // 输入初始化
    lv_port_fs_init();    // 文件接口初始化（可不要）
    rt_kprintf("LVGL init success\n");

    // 创建主界面线程，栈空间大一点，优先级最高
    top_gui_thread = rt_thread_create("top_gui_thread",
                                      top_gui_thread_entry,
                                      RT_NULL,
                                      THREAD_STACK_SIZE * 4,
                                      THREAD_PRIORITY - 3,
                                      THREAD_TIMESLICE);

    // 创建成功就启动主界面线程
    if (top_gui_thread != RT_NULL)
    {
        rt_thread_startup(top_gui_thread);
        rt_kprintf("top_gui_thread running...\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create top_gui_thread failed.\n");
        return -1;
    }
}
