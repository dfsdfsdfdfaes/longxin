//----------------------------------------------------------------------------
// 代码清单2：my_def.h
//----------------------------------------------------------------------------

#ifndef _MY_DEF_H
#define _MY_DEF_H

//----------------------------------------------------------------------------
// 简化书写的宏定义
//----------------------------------------------------------------------------
#define uchar unsigned char
//#define uint unsigned int

//----------------------------------------------------------------------------
// 与线程有关的宏
//----------------------------------------------------------------------------
#define THREAD_PRIORITY 25     // 线程优先级
#define THREAD_STACK_SIZE 1024 // 线程栈空间
#define THREAD_TIMESLICE 20    // 线程时间片

//----------------------------------------------------------------------------
// 与任务有关的枚举定义
//----------------------------------------------------------------------------
typedef enum
{
    TASK1,
    TASK2,
    TASK3,
    TASK4,
    TASK5,
    TASK6,
    TASK7,
    TASK8,
    NO_TASK
} TaskNum;
extern TaskNum task_num; // 主按钮索引（全工程适用）

typedef enum
{
    SUB_TASK_1,
    SUB_TASK_2,
    SUB_TASK_3,
    SUB_TASK_4,
    SUB_TASK_EXIT,
    SUB_TASK_NONE
} SubTaskNum;
extern SubTaskNum sub_task_num; // 二级按钮索引

typedef enum
{
    IMG_BTN_1,
    IMG_BTN_2,
    IMG_BTN_3,
    IMG_BTN_4,
    IMG_BTN_NONE
} ImgBtnNum;
extern ImgBtnNum img_btn_num; // 图像按钮索引

typedef enum
{
    NOTE_1,
    NOTE_2,
    NOTE_3,
    NOTE_4,
    NOTE_5,
    NOTE_6,
    NOTE_7,
    MUSIC_1,
    MUSIC_2,
    MUSIC_NONE
} PlayBtnNum;
extern PlayBtnNum play_btn_num; // 播放按钮索引
#endif
