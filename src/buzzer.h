
#ifndef BUZZER_H
#define BUZZER_H

#include "ls1b_gpio.h"
#include "ls1x_pwm.h"

//------------------------------------------
// 引脚（端口）宏定义
//------------------------------------------
#define BUZZER 28

//------------------------------------------
// IO口操作宏定义
//------------------------------------------
#define BUZZER_ON gpio_write(BUZZER, 1)
#define BUZZER_OFF gpio_write(BUZZER, 0)
#define BUZZER_TOG gpio_write(BUZZER, !gpio_read(BUZZER))

extern pwm_cfg_t buzzer_pwm;
// 播放枚举
typedef enum
{
    PLAY_NONE,
    PLAY_NOTE,
    PLAY_MUSIC1,
    PLAY_MUSIC2
} PlayFlag;
extern PlayFlag play_flag;
//------------------------------------------
// 函数声明
//------------------------------------------
void Buzzer_Init(void);
void BuzzerPWM_Init(void);

#endif
