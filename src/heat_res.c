
#include <stdbool.h>
#include "ls1b.h"
#include "ls1b_gpio.h"
#include "heat_res.h"

/*---------------------------------------------------
 * 函数名称：HeatRes_Init
 * 函数功能：加热电阻控制端口初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：初始化输出低电平
 * -------------------------------------------------*/
void HeatRes_Init(void)
{
    gpio_enable(HEAT_RES, DIR_OUT);
    HEAT_RES_OFF;
}
