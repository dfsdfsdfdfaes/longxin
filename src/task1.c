/*
 * task1.c
 *
 * created: 2023/12/15
 *  author:  ZLX
 */

//-----------------------------------------------------------
// 必要的头文件
//-----------------------------------------------------------
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "task1.h"
#include "rgb_led.h"
#include "key.h"
#include "buzzer.h"

//-----------------------------------------------------------
// RGB闪烁灯子线程（三级）控制块和启动标志
//-----------------------------------------------------------
static rt_thread_t rgb_blink_thread = RT_NULL;
static rt_bool_t rgb_blink_started = RT_FALSE;
//-----------------------------------------------------------
// 呼吸灯定时器控制块和启动标志
//-----------------------------------------------------------
static rt_thread_t rgb_breath_timer = RT_NULL;
static rt_bool_t rgb_breath_started = RT_FALSE;
//-----------------------------------------------------------
// 无源蜂鸣器嘀嘀嘀定时器
//-----------------------------------------------------------
rt_timer_t buzzer_tick_timer = RT_NULL;
rt_bool_t buzzer_tick_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：rgb_blink_thread_entry
 * 功    能：RGB闪烁灯子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：红亮 --- 灭 --- 绿亮 --- 灭 --- 蓝亮 --- 灭，间隔1s
 *---------------------------------------------------------------*/
static void rgb_blink_thread_entry(void *arg)
{
    rt_uint8_t count = 0; // 计数变量

    while (1)
    {
        count++;       // 计数累加
        switch (count) // 根据计数值决定RGB的状态
        {
        case 1:
            RED_ON;
            break;
        case 2:
            RED_OFF;
            break;
        case 3:
            GREEN_ON;
            break;
        case 4:
            GREEN_OFF;
            break;
        case 5:
            BLUE_ON;
            break;
        case 6:
            BLUE_OFF;
            count = 0;
            break;
        default:
            break;
        }
        rt_thread_mdelay(1000); // 线程中要用挂起延时
    }
}

/*----------------------------------------------------------------
 * 函数名称：rgb_blink_thread_init
 * 功    能：RGB闪烁灯子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int rgb_blink_thread_init(void)
{
    // 创建子线程，栈空间不大，优先级比主线程低
    rgb_blink_thread = rt_thread_create("rgb_blink_thread",
                                        rgb_blink_thread_entry,
                                        RT_NULL,
                                        THREAD_STACK_SIZE,
                                        THREAD_PRIORITY - 1,
                                        THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (rgb_blink_thread != RT_NULL)
    {
        rt_thread_startup(rgb_blink_thread);
        rt_kprintf("rgb_blink_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create rgb_blink_thread failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------
 * 函数名称：rgb_breath_timer_cb
 * 功    能：RGB呼吸灯定时器回调函数
 * 入口参数：按照定时器回调函数格式
 * 返 回 值：无
 * 说明描述：1ms执行一次，调整占空比和持续时长
 *---------------------------------------------------------------*/
static void rgb_breath_timer_cb(void *arg)
{
    static rt_uint16_t pwm_time = 0;         // 用于累加，直到所需的占空比
    static rt_uint16_t breath_time = 0;      // 呼（渐亮）或吸（渐暗）的时长
    static rt_uint16_t pwm_value = 0;        // 占空比大小
    static rt_bool_t breath_flag = RT_FALSE; // 吸气或呼气的标志

    pwm_time++;    // 累加一次占空比计数
    breath_time++; // 累加一次呼气或吸气的时长

    if (pwm_time == pwm_value) // 达到占空比点亮
        GREEN_ON;

    if (pwm_time == 10) // 当前周期结束
    {
        GREEN_OFF;    // 未达到占空比熄灭
        pwm_time = 0; // 重新计时，去改变占空比
    }

    if (breath_time == 250 && !breath_flag) // 250ms改变一次占空比
    {
        breath_time = 0;
        pwm_value++; // 改变占空比（累加）

        if (pwm_value == 10) // 加到上限改变呼吸方向
            breath_flag = RT_TRUE;
    }

    if (breath_time == 250 && breath_flag) // 250ms改变一次占空比
    {
        breath_time = 0;
        pwm_value--; // 改变占空比（递减）

        if (pwm_value == 1) // 减到下限改变呼吸方向
            breath_flag = RT_FALSE;
    }
}

/*----------------------------------------------------------------
 * 函数名称：rgb_breath_timer_init
 * 功    能：RGB呼吸灯定时器的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：周期性软件定时器，1ms
 *---------------------------------------------------------------*/
int rgb_breath_timer_init(void)
{
    // 创建定时器
    rgb_breath_timer = rt_timer_create("rgb_breath_timer",
                                       rgb_breath_timer_cb,
                                       NULL,
                                       1,
                                       RT_TIMER_FLAG_PERIODIC |
                                           RT_TIMER_FLAG_SOFT_TIMER);

    // 创建成功则启动定时器
    if (rgb_breath_timer != RT_NULL)
    {
        rt_timer_start(rgb_breath_timer);
        rt_kprintf("rgb_breath_timer running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create rgb_breath_timer failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------
 * 函数名称：buzzer_tick_timer_cb
 * 功    能：无源蜂鸣器嘀嘀嘀定时器回调函数
 * 入口参数：按照定时器回调函数格式
 * 返 回 值：无
 * 说明描述：控制无源蜂鸣器的IO口1ms变化一次电平，即对应500Hz脉冲，
 *           持续500ms后关闭蜂鸣器，如此重复即发出嘀嘀嘀声音
 *---------------------------------------------------------------*/
void buzzer_tick_timer_cb(void *arg)
{
    static rt_uint16_t count = 0; // 定时次数计数

    count++;
    if (count <= 500) // 前500ms，产生脉冲发出嘀的声音
    {
        BUZZER_TOG;
    }
    else if (count <= 1000) // 后500ms，关闭蜂鸣器
    {
        BUZZER_OFF;
    }
    else // 清零，后续重复上述过程
    {
        count = 0;
    }
}

/*----------------------------------------------------------------
 * 函数名称：buzzer_tick_timer_init
 * 功    能：蜂鸣器嘀嘀嘀定时器的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：周期性软件定时器，1ms
 *---------------------------------------------------------------*/
int buzzer_tick_timer_init(void)
{
    // 创建定时器
    buzzer_tick_timer = rt_timer_create("buzzer_tick_timer",
                                        buzzer_tick_timer_cb,
                                        NULL,
                                        1,
                                        RT_TIMER_FLAG_PERIODIC |
                                            RT_TIMER_FLAG_SOFT_TIMER);

    // 创建成功则启动定时器
    if (buzzer_tick_timer != RT_NULL)
    {
        rt_timer_start(buzzer_tick_timer);
        rt_kprintf("buzzer_tick_timer running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create buzzer_tick_timer failed.\n");
        return -1;
    }
}

//-----------------------------------------------------------
// 任务1子线程（二级）控制块
//-----------------------------------------------------------
rt_thread_t task1_thread = RT_NULL;

/*----------------------------------------------------------------
 * 函数名称：task1_thread_entry
 * 功    能：任务1子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务1下的各子任务（线程）
 *---------------------------------------------------------------*/
static void task1_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK1) // 主按钮为任务1
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1:            // 子任务1.1按钮
                if (!rgb_blink_started) // 如果闪烁灯未启动
                {
                    if (rgb_breath_started) // 如果呼吸灯已启动
                    {
                        rt_timer_delete(rgb_breath_timer); // 先结束呼吸灯
                        rgb_breath_started = RT_FALSE;     // 呼吸灯标志清零
                    }
                    RGB_OFF;                     // 闪烁灯之前先灭灯
                    rgb_blink_thread_init();     // 启动闪烁灯子线程
                    rgb_blink_started = RT_TRUE; // 闪烁灯标志置位
                }
                break;

            case SUB_TASK_2:             // 子任务1.2按钮
                if (!rgb_breath_started) // 如果呼吸灯未启动
                {
                    if (rgb_blink_started) // 如果闪烁灯已启动
                    {
                        rt_thread_delete(rgb_blink_thread); // 先结束闪烁灯
                        rgb_blink_started = RT_FALSE;       // 闪烁灯标志清零
                    }
                    RGB_OFF;                      // 呼吸灯之前先灭灯
                    rgb_breath_timer_init();      // 启动呼吸灯定时器
                    rgb_breath_started = RT_TRUE; // 呼吸灯标志置位
                }
                break;

            case SUB_TASK_3:        // 子任务1.3按钮
                if (key1_ctrl_flag) // 如果按键1控制标志为真
                {
                    if (!buzzer_tick_started) // 且尚未启动蜂鸣器嘀嘀嘀
                    {
                        buzzer_tick_timer_init();      // 那么启动嘀嘀嘀定时器
                        buzzer_tick_started = RT_TRUE; // 蜂鸣器嘀嘀嘀标志置位
                    }
                }
                else // 如果按键1控制标志为假
                {
                    if (buzzer_tick_started) // 且已启动蜂鸣器嘀嘀嘀
                    {
                        rt_timer_delete(buzzer_tick_timer); // 那么结束嘀嘀嘀定时器
                        BUZZER_OFF;                         // 关闭蜂鸣器
                        buzzer_tick_started = RT_FALSE;     // 蜂鸣器嘀嘀嘀标志清零
                    }
                }
                break;

            case SUB_TASK_EXIT:        // 任务1退出按钮
                if (rgb_blink_started) // 如果闪烁灯已启动
                {
                    rt_thread_delete(rgb_blink_thread); // 结束闪烁灯子线程
                    rgb_blink_started = RT_FALSE;       // 闪烁灯标志清零
                }
                if (rgb_breath_started) // 如果呼吸灯已启动
                {
                    rt_timer_delete(rgb_breath_timer); // 结束呼吸灯定时器
                    rgb_breath_started = RT_FALSE;     // 呼吸灯标志清零
                }
                // 注：该if代码段为新增部分
                if (buzzer_tick_started) // 如果蜂鸣器嘀嘀嘀已启动
                {
                    rt_timer_delete(buzzer_tick_timer); // 结束蜂鸣器嘀嘀嘀定时器
                    buzzer_tick_started = RT_FALSE;     // 蜂鸣器嘀嘀嘀标志清零
                    key1_ctrl_flag = false;             // 按键1控制标志清零
                }
                RGB_OFF;                      // 灭灯
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task1_thread_init
 * 功    能：任务1子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task1_thread_init(void)
{
    // 创建任务1子线程，栈空间略大，优先级略高
    task1_thread = rt_thread_create("task1_thread",
                                    task1_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 2,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务1子线程
    if (task1_thread != RT_NULL)
    {
        rt_thread_startup(task1_thread);
        rt_kprintf("task1_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task1_thread failed.\n");
        return -1;
    }
}
