

//----------------------------------------------------------------------
// 必要的头文件
//----------------------------------------------------------------------
#include <stdio.h>
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "gui_layout.h"
#include "task4.h"
#include "lm35_adc.h"
#include "ls1b_gpio.h"
#include "pwm_ic.h"
#include "key.h"
#include "heat_res.h"
#include "bh1750.h"
#include "rgb_led.h"
#include "bkrc_voice.h"

#define CHART_POINTS 8    // 图表数据点个数
#define CHART_PERIOD 2000 // 图标刷新周期
//----------------------------------------------------------------------
// 全局变量
//----------------------------------------------------------------------
static lv_obj_t *temper_label; // 温度标签
static lv_obj_t *heat_label;   // 加热提示标签

static lv_obj_t *light_label;        // 光照标签
static uint light_lx, last_light_lx; // 当前和上一次光照值

static lv_obj_t *light_chart;                     // 光照图标对象
static lv_chart_series_t *light_series;           // 光照数据系列
static lv_coord_t light_data[CHART_POINTS] = {0}; // 最近几次的光照数据
static lv_style_t chart_style;                    // 图表样式对象
static lv_task_t *light_chart_refresh;            // 图标刷新任务

/*----------------------------------------------------------------------
 * 代码清单2：BKRCspeak_Temper函数，放在“子任务4.1”线程之前
 * 功    能：播报温度
 * 入口参数：tcs --- 温度值
 * 返 回 值：无
 * 说明描述：拆分十位、各位和小数，分别播报
 *--------------------------------------------------------------------*/
void BKRCspeak_Temper(float tcs)
{
    rt_uint8_t tens, uints, points; // 十位/个位/小数

    tens = (int)tcs / 10; // 根据温度值取出每位数字
    uints = (int)tcs % 10;
    points = (int)((tcs - (int)tcs) * 10);

    if (tens == 2) // 十位数为2
    {
        switch (uints) // 再根据个位数播报整数部分
        {
        case 0:
            BKRCspeak_TTS("二十点");
            break;
        case 1:
            BKRCspeak_TTS("二十一点");
            break;
        case 2:
            BKRCspeak_TTS("二十二点");
            break;
        case 3:
            BKRCspeak_TTS("二十三点");
            break;
        case 4:
            BKRCspeak_TTS("二十四点");
            break;
        case 5:
            BKRCspeak_TTS("二十五点");
            break;
        case 6:
            BKRCspeak_TTS("二十六点");
            break;
        case 7:
            BKRCspeak_TTS("二十七点");
            break;
        case 8:
            BKRCspeak_TTS("二十八点");
            break;
        case 9:
            BKRCspeak_TTS("二十九点");
            break;
        default:
            break;
        }
    }

    else if (tens == 3) // 十位数为3
    {
        switch (uints)
        {
        case 0:
            BKRCspeak_TTS("三十点");
            break;
        case 1:
            BKRCspeak_TTS("三十一点");
            break;
        case 2:
            BKRCspeak_TTS("三十二点");
            break;
        case 3:
            BKRCspeak_TTS("三十三点");
            break;
        case 4:
            BKRCspeak_TTS("三十四点");
            break;
        case 5:
            BKRCspeak_TTS("三十五点");
            break;
        case 6:
            BKRCspeak_TTS("三十六点");
            break;
        case 7:
            BKRCspeak_TTS("三十七点");
            break;
        case 8:
            BKRCspeak_TTS("三十八点");
            break;
        case 9:
            BKRCspeak_TTS("三十九点");
            break;
        default:
            break;
        }
    }

    else if (tens == 1) // 十位数为1
    {
        switch (uints)
        {
        case 0:
            BKRCspeak_TTS("十点");
            break;
        case 1:
            BKRCspeak_TTS("十一点");
            break;
        case 2:
            BKRCspeak_TTS("十二点");
            break;
        case 3:
            BKRCspeak_TTS("十三点");
            break;
        case 4:
            BKRCspeak_TTS("十四点");
            break;
        case 5:
            BKRCspeak_TTS("十五点");
            break;
        case 6:
            BKRCspeak_TTS("十六点");
            break;
        case 7:
            BKRCspeak_TTS("十七点");
            break;
        case 8:
            BKRCspeak_TTS("十八点");
            break;
        case 9:
            BKRCspeak_TTS("十九点");
            break;
        default:
            break;
        }
    }

    else if (tens == 4) // 十位数为4
    {
        switch (uints)
        {
        case 0:
            BKRCspeak_TTS("四十点");
            break;
        case 1:
            BKRCspeak_TTS("四十一点");
            break;
        case 2:
            BKRCspeak_TTS("四十二点");
            break;
        case 3:
            BKRCspeak_TTS("四十三点");
            break;
        case 4:
            BKRCspeak_TTS("四十四点");
            break;
        case 5:
            BKRCspeak_TTS("四十五点");
            break;
        case 6:
            BKRCspeak_TTS("四十六点");
            break;
        case 7:
            BKRCspeak_TTS("四十七点");
            break;
        case 8:
            BKRCspeak_TTS("四十八点");
            break;
        case 9:
            BKRCspeak_TTS("四十九点");
            break;
        default:
            break;
        }
    }

    switch (points) // 播报小数
    {
    case 0:
        BKRCspeak_TTS("零摄氏度");
        break;
    case 1:
        BKRCspeak_TTS("一摄氏度");
        break;
    case 2:
        BKRCspeak_TTS("二摄氏度");
        break;
    case 3:
        BKRCspeak_TTS("三摄氏度");
        break;
    case 4:
        BKRCspeak_TTS("四摄氏度");
        break;
    case 5:
        BKRCspeak_TTS("五摄氏度");
        break;
    case 6:
        BKRCspeak_TTS("六摄氏度");
        break;
    case 7:
        BKRCspeak_TTS("七摄氏度");
        break;
    case 8:
        BKRCspeak_TTS("八摄氏度");
        break;
    case 9:
        BKRCspeak_TTS("九摄氏度");
        break;
    default:
        break;
    }
}

//----------------------------------------------------------------------
// 光照图表曲线子线程（子任务4.4）控制块、启动标志和入口函数
//----------------------------------------------------------------------
static rt_thread_t light_chart_thread = RT_NULL;
static rt_bool_t light_chart_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：light_chart_thread_entry
 * 功    能：光照图表曲线子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：生成图表界面，定时挂起
 *--------------------------------------------------------------------*/
static void light_chart_thread_entry(void *arg)
{
    light_chart_gui(); // 生成图表界面

    while (1)
    {
        rt_thread_mdelay(100);
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：light_chart_thread_init
 * 功    能：光照图表曲线子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int light_chart_thread_init(void)
{
    // 创建子线程，优先级比任务4线程低
    light_chart_thread = rt_thread_create("light_chart_thread",
                                          light_chart_thread_entry,
                                          RT_NULL,
                                          THREAD_STACK_SIZE * 2,
                                          THREAD_PRIORITY - 1,
                                          THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (light_chart_thread != RT_NULL)
    {
        rt_thread_startup(light_chart_thread);
        rt_kprintf("light_chart_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create light_chart_thread failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------------
 * 函数名称：update_light_chart
 * 功    能：更新光照曲线的回调函数
 * 入口参数：按照任务回调函数格式
 * 返 回 值：无
 * 说明描述：后面会创建一个LVGL任务对象，负责刷新图表，会调用本函数
 *--------------------------------------------------------------------*/
void update_light_chart(lv_task_t *task)
{
    // 数据索引和循环下标
    static rt_uint8_t data_i = 0, k;

    // 更新光照数据（数组元素从后往前依次覆盖更新）
    // data_i为0，不更新
    // data_i为1，更新1次：[6]<-[7]
    // data_i为2，更新2次：[5]<-[6]，[6]<-[7]
    // data_i为3，更新3次：[4]<-[5]，[5]<-[6]，[6]<-[7]
    // 后面以此类推，data_i最大到7
    for (k = 0; k < data_i; k++)
    {
        light_data[CHART_POINTS - 1 - data_i + k] =
            light_data[CHART_POINTS - data_i + k];
    }
    // 将最新的光照数据填入最后一个元素
    light_data[CHART_POINTS - 1] = light_lx;

    // 将数据送上图表并显示
    lv_chart_set_points(light_chart, light_series, light_data);
    lv_chart_refresh(light_chart);

    // 更新依次数据索引
    if (++data_i == CHART_POINTS)
        data_i = 0;
}

/*----------------------------------------------------------------------
 * 函数名称：light_chart_gui
 * 功    能：生成光照图表曲线的界面
 * 入口参数：无
 * 返 回 值：无
 * 说明描述：创建图表、设置格式
 *--------------------------------------------------------------------*/
void light_chart_gui(void)
{
    // 创建一个图表，在光照标签下面，尺寸不超过屏幕剩余空间
    light_chart = lv_chart_create(lv_scr_act(), NULL);
    lv_obj_align(light_chart, light_label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 20);
    lv_obj_set_size(light_chart, 380, 300);

    // x轴和y轴的网格线数量、数据点、以及y轴数据范围
    lv_chart_set_div_line_count(light_chart, 4, CHART_POINTS - 1);
    lv_chart_set_point_count(light_chart, CHART_POINTS);
    lv_chart_set_range(light_chart, 0, 1200);

    // 准备图表刻度标签的样式
    lv_style_set_pad_bottom(&chart_style, LV_STATE_DEFAULT, 40);
    lv_obj_add_style(light_chart, LV_CHART_PART_BG, &chart_style);

    // x轴的刻度线和标签
    lv_chart_set_x_tick_length(light_chart, 1, 0);
    lv_chart_set_x_tick_texts(light_chart, "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15",
                              0, LV_CHART_AXIS_DRAW_LAST_TICK);
    // y轴的刻度线和标签
    lv_chart_set_y_tick_length(light_chart, 200, 0);
    lv_chart_set_y_tick_texts(light_chart, "1200\n1000\n800\n600\n400\n200\n0",
                              0, LV_CHART_AXIS_DRAW_LAST_TICK);

    // 设置图表类型为线性图表
    lv_chart_set_type(light_chart, LV_CHART_TYPE_LINE);

    // 创建光照数据系列
    light_series = lv_chart_add_series(light_chart, LV_COLOR_ORANGE);

    // 定期执行更新曲线的任务（回调函数）
    light_chart_refresh = lv_task_create(update_light_chart, CHART_PERIOD,
                                         LV_TASK_PRIO_LOW, NULL);
}

//----------------------------------------------------------------------
// 光照显示与控制子线程（子任务4.3）控制块、启动标志和入口函数
//----------------------------------------------------------------------
static rt_thread_t light_thread = RT_NULL;
static rt_bool_t light_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：light_thread_entry
 * 功    能：光照显示与控制子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：使用标签文字显示“当前环境光强度：xxx lx”，
 *           当光照度<100时，点亮白灯，反之关闭白灯
 *--------------------------------------------------------------------*/
static void light_thread_entry(void *arg)
{
    char light_str[50]; // 光照标签的字符串

    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备显示光照的标签，绝对坐标，位于加热电阻标签下面
    light_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(light_label, 250);
    lv_obj_align(light_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 100);
    lv_obj_add_style(light_label, LV_LABEL_PART_MAIN, &style_label);

    while (1)
    {
        last_light_lx = light_lx; // 保存上一次光照值

        // 获取当前光照值
        BH1750_ReadData();
        light_lx = BH1750_Convert();

        // 把光照值格式化成字符串
        snprintf(light_str, sizeof(light_str),
                 ""
                 "\xE5\xBD\x93" /*当*/ ""
                 "\xE5\x89\x8D" /*前*/
                 ""
                 "\xE7\x8E\xAF" /*环*/ ""
                 "\xE5\xA2\x83" /*境*/
                 ""
                 "\xE5\x85\x89" /*光*/ ""
                 "\xE5\xBC\xBA" /*强*/
                 ""
                 "\xE5\xBA\xA6" /*度*/ ": %d lx",
                 light_lx);

        // 通过标签显示光照
        lv_label_set_align(light_label, LV_LABEL_ALIGN_LEFT);
        lv_label_set_text(light_label, light_str);

        // 连续两次的光照值低于100才开灯，避免在临界值处频繁开关灯
        // 同理，连续两次的光照值高于100才关灯
        if (light_lx <= 100 && last_light_lx <= 100)
        {
            RGB_ON;
        }
        else if (light_lx > 100 && last_light_lx > 100)
        {
            RGB_OFF;
        }
        else
        {
        }

        // 每1s刷新测量一次光照
        rt_thread_mdelay(1000);
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：light_thread_init
 * 功    能：光照显示与控制子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int light_thread_init(void)
{
    // 创建子线程，优先级比任务4线程低
    light_thread = rt_thread_create("light_thread",
                                    light_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 2,
                                    THREAD_PRIORITY - 1,
                                    THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (light_thread != RT_NULL)
    {
        rt_thread_startup(light_thread);
        rt_kprintf("light_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create light_thread failed.\n");
        return -1;
    }
}
//-----------------------------------------------------------
// 加热电阻子线程（子任务4.2）控制块、启动标志和入口函数
//-----------------------------------------------------------
static rt_thread_t heat_thread = RT_NULL;
static rt_bool_t heat_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：heat_thread_entry
 * 功    能：加热电阻子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：使用标签文字显示“加热电阻：关/开”，由单击KEY2改变
 *--------------------------------------------------------------------*/
static void heat_thread_entry(void *arg)
{
    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备加热电阻提示信息的标签，绝对坐标，位于温度标签下方
    heat_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(heat_label, 250);
    lv_obj_align(heat_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 60);
    lv_obj_add_style(heat_label, LV_LABEL_PART_MAIN, &style_label);

    while (1)
    {
        if (!key2_ctrl_flag) // KEY2控制标志为0
        {
            HEAT_RES_OFF; // 关闭加热电阻并显示
            lv_label_set_align(heat_label, LV_LABEL_ALIGN_LEFT);
            lv_label_set_text(heat_label,
                              ""
                              "\xE5\x8A\xA0" /*加*/
                              ""
                              "\xE7\x83\xAD" /*热*/
                              ""
                              "\xE7\x94\xB5" /*电*/
                              ""
                              "\xE9\x98\xBB" /*阻*/
                              ": "
                              "\xE5\x85\xB3" /*关*/ "");
        }
        else // KEY2控制标志为1
        {
            HEAT_RES_ON; // 启动加热电阻并显示
            lv_label_set_align(heat_label, LV_LABEL_ALIGN_LEFT);
            lv_label_set_text(heat_label,
                              ""
                              "\xE5\x8A\xA0" /*加*/
                              ""
                              "\xE7\x83\xAD" /*热*/
                              ""
                              "\xE7\x94\xB5" /*电*/
                              ""
                              "\xE9\x98\xBB" /*阻*/
                              ": "
                              "\xE5\xBC\x80" /*开*/ "");
        }

        rt_thread_mdelay(100); // 该线程100ms挂起一次
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：heat_thread_init
 * 功    能：加热电阻子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int heat_thread_init(void)
{
    // 创建子线程，优先级比任务4线程低
    heat_thread = rt_thread_create("heat_thread",
                                   heat_thread_entry,
                                   RT_NULL,
                                   THREAD_STACK_SIZE * 2,
                                   THREAD_PRIORITY - 1,
                                   THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (heat_thread != RT_NULL)
    {
        rt_thread_startup(heat_thread);
        rt_kprintf("heat_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create heat_thread failed.\n");
        return -1;
    }
}

//----------------------------------------------------------------------
// 温度实时显示子线程（子任务4.1）控制块和启动标志
//----------------------------------------------------------------------
static rt_thread_t show_temper_thread = RT_NULL;
static rt_bool_t show_temper_started = RT_FALSE;

/*----------------------------------------------------------------------
 * 函数名称：show_temper_thread_entry
 * 功    能：温度实时显示子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：使用标签文字显示“当前温度：xx.x ℃”，每2s更新一次
 *--------------------------------------------------------------------*/
static void show_temper_thread_entry(void *arg)
{
    float temper;        // 温度值
    char temper_str[50]; // 温度标签的字符串

    // 准备标签字体效果（初始化、颜色、大小）
    lv_style_init(&style_label);
    lv_style_set_text_color(&style_label, LV_STATE_DEFAULT, LV_COLOR_ORANGE);

    // 准备显示标签，放在屏幕上部，并将标签样式应用之上
    temper_label = lv_label_create(lv_scr_act(), NULL);
    lv_obj_set_width(temper_label, 250);
    lv_obj_align(temper_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -360, 20);
    lv_obj_add_style(temper_label, LV_LABEL_PART_MAIN, &style_label);

    // gpio_enable(40,DIR_OUT);
    // gpio_enable(41,DIR_OUT);
    // gpio_write(41,1);   //加热电阻打开
    // gpio_write(40,1);   //风扇开关打开

    PCA_MG9XX_Init(50, 10); // 初始化PWM/舵机驱动
    pca_setpwm(4, 0, 4000);

    while (1)
    {

        // 调用测温函数得到温度值
        temper = LM35_Test();

        // 把温度值格式化成字符串
        snprintf(temper_str, sizeof(temper_str),
                 ""
                 "\xE5\xBD\x93" /*当*/ ""
                 "\xE5\x89\x8D" /*前*/
                 ""
                 "\xE6\xB8\xA9" /*温*/ ""
                 "\xE5\xBA\xA6" /*度*/
                 ": %.2f "
                 "\xE2\x84\x83" /*℃*/ "",
                 temper);

        // 通过标签显示温度
        lv_label_set_align(temper_label, LV_LABEL_ALIGN_LEFT);
        lv_label_set_text(temper_label, temper_str);

        UART4_Test();
        if (BKRC_VoiceDrive() == 0x18 && show_temper_started)
        {
            rt_thread_mdelay(1500);
            BKRCspeak_Temper(temper);
        }

        // 每1s刷新测量一次温度
        rt_thread_mdelay(1000);
    }
}

/*-----------------------------------------------------------------------
 * 函数名称：show_temper_thread_init
 * 功    能：温度实时显示子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------------*/
int show_temper_thread_init(void)
{
    // 创建子线程，优先级比任务4线程低
    show_temper_thread = rt_thread_create("show_temper_thread",
                                          show_temper_thread_entry,
                                          RT_NULL,
                                          THREAD_STACK_SIZE * 2,
                                          THREAD_PRIORITY - 1,
                                          THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (show_temper_thread != RT_NULL)
    {
        rt_thread_startup(show_temper_thread);
        rt_kprintf("show_temper_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create show_temper_thread failed.\n");
        return -1;
    }
}

//---------------------------------------------------------------------
// 任务4子线程（二级）控制块
//---------------------------------------------------------------------
rt_thread_t task4_thread = RT_NULL;

/*---------------------------------------------------------------------
 * 函数名称：task4_thread_entry
 * 功    能：任务4子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务4下的各子任务（线程）
 *-------------------------------------------------------------------*/
static void task4_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK4) // 主按钮为任务4
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1: // 子任务4.1按钮
                if (!show_temper_started)
                {
                    show_temper_thread_init();     // 启动温度显示子线程
                    show_temper_started = RT_TRUE; // 温度显示子线程标志置位
                }
                break;

            case SUB_TASK_2:
                if (!heat_started)
                {
                    heat_thread_init();
                    heat_started = RT_TRUE;
                }
                break;

            case SUB_TASK_3:
                if (!light_started)
                {
                    light_thread_init();
                    light_started = RT_TRUE;
                }
                break;

            case SUB_TASK_4:
                if (light_started && !light_chart_started)
                {
                    light_chart_thread_init();
                    light_chart_started = RT_TRUE;
                }
                break;

            case SUB_TASK_EXIT:          // 任务4退出按钮
                if (show_temper_started) // 子任务4.1启动标志为真则退出
                {
                    lv_obj_del(temper_label);
                    rt_thread_delete(show_temper_thread);
                    show_temper_started = RT_FALSE;
                    gpio_write(41, 0); // 加热电阻关闭
                    gpio_write(40, 0); // 风扇开关关闭
                }
                if (heat_started)
                {
                    lv_obj_del(heat_label);
                    rt_thread_delete(heat_thread);
                    heat_started = RT_FALSE;
                }
                if (light_started)
                {
                    lv_obj_del(light_label);
                    rt_thread_delete(light_thread);
                    light_started = RT_FALSE;
                }
                if (light_chart_started)
                {
                    lv_chart_clear_serie(light_chart, light_series);
                    lv_task_del(light_chart_refresh);
                    lv_obj_del(light_chart);
                    rt_thread_delete(light_chart_thread);
                    light_chart_started = RT_FALSE;
                }
                HEAT_RES_OFF;
                sub_task_num = SUB_TASK_NONE; // 子任务按钮索引为无关数
                break;

            default:
                break;
            }
        }

        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task4_thread_init
 * 功    能：任务4子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task4_thread_init(void)
{
    // 创建任务4子线程，栈空间加大点，为后续图形显示的子任务预留
    task4_thread = rt_thread_create("task4_thread",
                                    task4_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 4,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务4子线程
    if (task4_thread != RT_NULL)
    {
        rt_thread_startup(task4_thread);
        rt_kprintf("task4_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task4_thread failed.\n");
        return -1;
    }
}
