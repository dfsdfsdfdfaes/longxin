

#include "ls1b.h"
#include "ls1b_gpio.h"
#include "rgb_led.h"

/* --------------------------------------------------
 * 函数名称：RGB_Init
 * 函数功能：RGB三色灯IO口初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：无
 * ------------------------------------------------*/
void RGB_Init(void)
{
    // 三个IO口都配置成输出模式
    gpio_enable(RED_LED, DIR_OUT);
    gpio_enable(GREEN_LED, DIR_OUT);
    gpio_enable(BLUE_LED, DIR_OUT);

    // 初始都熄灭
    RGB_OFF;
}
