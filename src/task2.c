
//-----------------------------------------------------------
// 必要的头文件
//-----------------------------------------------------------
#include "rtthread.h"
#include "my_def.h"
#include "rtt_lvgl_top.h"
#include "task1.h"
#include "buzzer.h"
#include "task2.h"
#include "seg595.h"
#include "matrix.h"
#include "font.h"

#define COUNT_DOWN 10 // 倒计时秒数
//-----------------------------------------------------------
// 显示内容的段码组合
//-----------------------------------------------------------
// 工号：no28
#define SHOW_WORK_NO               \
    {                              \
        disp_tab[0] = 0x54;        \
        disp_tab[1] = 0x5c;        \
        disp_tab[2] = num_code[2]; \
        disp_tab[3] = num_code[8]; \
    }
// 年份：2023
#define SHOW_YEAR                  \
    {                              \
        disp_tab[0] = num_code[2]; \
        disp_tab[1] = num_code[0]; \
        disp_tab[2] = num_code[2]; \
        disp_tab[3] = num_code[3]; \
    }
// 日期：0910
#define SHOW_DATE                  \
    {                              \
        disp_tab[0] = num_code[0]; \
        disp_tab[1] = num_code[9]; \
        disp_tab[2] = num_code[1]; \
        disp_tab[3] = num_code[0]; \
    }
// 无显示
#define SHOW_NONE           \
    {                       \
        disp_tab[0] = 0x00; \
        disp_tab[1] = 0x00; \
        disp_tab[2] = 0x00; \
        disp_tab[3] = 0x00; \
    }

// 带参数的数字变换格式
#define SHOW_NUM(xx)                     \
    {                                    \
        disp_tab[0] = 0x40;              \
        disp_tab[1] = num_code[xx / 10]; \
        disp_tab[2] = num_code[xx % 10]; \
        disp_tab[3] = 0x40;              \
    }

// 倒计时结束格式
#define SHOW_END            \
    {                       \
        disp_tab[0] = 0x40; \
        disp_tab[1] = 0x71; \
        disp_tab[2] = 0x71; \
        disp_tab[3] = 0x40; \
    }
    
// 点阵显示样式
/*unsigned char PL[32] =
{

0xFF,0xFF,0xFF,0xFF,0x3F,0xFC,0xDF,0xFB,0xDF,0xFB,0xDF,0xFB,0x3F,0xFC,0x7F,0xFE,
0x07,0xE0,0x7F,0xFE,0x7F,0xFE,0xBF,0xFD,0xDF,0xFB,0xEF,0xF7,0xFF,0xFF,0xFF,0xFF,

};*/

static rt_int8_t sec = COUNT_DOWN; // 变换的秒数

//-----------------------------------------------------------
// 数码管显示定时器的控制块和启动标志
//-----------------------------------------------------------
static rt_timer_t seg_disp_timer = RT_NULL;
static rt_bool_t seg_disp_started = RT_FALSE;
//点阵显示的控制块和启动标志
static rt_thread_t matrix_light_thread = RT_NULL;
static rt_bool_t matrix_light_started = RT_FALSE;

/*----------------------------------------------------------------
 * 函数名称：seg_disp_timer_cb
 * 功   能：数码管显示定时器回调函数
 * 入口参数：按照定时器回调函数格式
 * 返 回 值：无
 * 说明描述：2ms执行一次，刷新一位数码管的显示
 *---------------------------------------------------------------*/
static void seg_disp_timer_cb(void *arg)
{
    static rt_uint16_t count2 = 0, count3 = 0; // 定时次数，用于子任务2.2的交替时间

    HC595_Display(1, 4); // 刷新一次数码管显示

    if (sub_task_num == SUB_TASK_1) // 如果是子任务2.1
    {
        count2 = 0;   // 子任务2.2的定时次数清0
        SHOW_WORK_NO; // 显示工号
    }

    else if (sub_task_num == SUB_TASK_2) // 如果是子任务2.2
    {
        count2++;           // 累加定时次数
        if (count2 <= 1000) // 前1s显示年份
        {
            SHOW_YEAR;
        }
        else if (count2 <= 2000) // 后1s显示日期
        {
            SHOW_DATE;
        }
        else // 完成1次交替显示后，定时次数清0
        {
            count2 = 0;
        }
    }

    else if (sub_task_num == SUB_TASK_3)
    {
        count2 = 0;
        count3++;
        if (count3 == 500 && sec >= 0)
        {
            SHOW_NUM(sec);
            sec--;
            count3 = 0;
        }
        // 剩余5秒闪烁
        else if (count3 == 250 && sec >= 0 && sec < 5)
        {
            SHOW_NONE;
        }
        else if (count3 == 500 && sec < 0)
        {
            SHOW_END;
            count3 = 0;
        }
    }
}


/*----------------------------------------------------------------
 * 函数名称：matrix_light_thread_entry
 * 功    能：点阵显示子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：点阵屏显示指定样式
 *---------------------------------------------------------------*/
static void matrix_light_thread_entry(void *arg)
{
    unsigned int i;
    for(i = 0; i < 2200; i++)
        {
            Dis_Char(PL);
            delay_ms(1);
        }
    for(;;)
    {
        for(i = 0; i < 64; i++)
        {
            Dis_1_Roll(Roll_buff,sizeof(Roll_buff));
        }
    }
        rt_thread_mdelay(1000); // 线程中要用挂起延时
}

/*----------------------------------------------------------------
 * 函数名称：matrix_light_thread_init
 * 功    能：点阵显示子线程的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int matrix_light_thread_init(void)
{
    // 创建子线程，栈空间不大，优先级比主线程低
    matrix_light_thread = rt_thread_create("matrix_light_thread",
                                        matrix_light_thread_entry,
                                        RT_NULL,
                                        THREAD_STACK_SIZE,
                                        THREAD_PRIORITY - 1,
                                        THREAD_TIMESLICE);

    // 创建成功就启动子线程
    if (matrix_light_thread != RT_NULL)
    {
        rt_thread_startup(matrix_light_thread);
        rt_kprintf("matrix_light_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create matrix_light_thread failed.\n");
        return -1;
    }
}

/*----------------------------------------------------------------
 * 函数名称：seg_disp_timer_init
 * 功    能：数码管显示定时器的创建和启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：周期性软件定时器，2ms
 *---------------------------------------------------------------*/
int seg_disp_timer_init(void)
{
    // 创建定时器
    seg_disp_timer = rt_timer_create("seg_disp_timer",
                                     seg_disp_timer_cb,
                                     NULL,
                                     2,
                                     RT_TIMER_FLAG_PERIODIC |
                                         RT_TIMER_FLAG_SOFT_TIMER);

    // 创建成功则启动定时器
    if (seg_disp_timer != RT_NULL)
    {
        rt_timer_start(seg_disp_timer);
        rt_kprintf("seg_disp_timer running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create seg_disp_timer failed.\n");
        return -1;
    }
}

//-----------------------------------------------------------
// 任务2子线程（二级）控制块
//-----------------------------------------------------------
rt_thread_t task2_thread = RT_NULL;

/*----------------------------------------------------------------
 * 代码清单5：修改后的task2_thread_entry函数
 * 功    能：任务2子线程的入口函数
 * 入口参数：按照线程入口函数格式
 * 返 回 值：无
 * 说明描述：管理任务2下的各子任务（线程）
 *---------------------------------------------------------------*/
static void task2_thread_entry(void *arg)
{
    while (1)
    {
        if (task_num == TASK2) // 主按钮为任务2
        {
            switch (sub_task_num) // 根据二级按钮索引
            {
            case SUB_TASK_1:           // 子任务2.1按钮
            case SUB_TASK_2:           // 或子任务2.2按钮
            case SUB_TASK_3:           // 或子任务2.3按钮
                if (!seg_disp_started) // 如果数码管显示未启动
                {
                    seg_disp_timer_init();      // 启动数码管显示
                    seg_disp_started = RT_TRUE; // 数码管显示标志置位
                }
                if (!buzzer_tick_started && sec < 0) // 蜂鸣器定时器未启动
                {                                    // 且倒计时结束
                    buzzer_tick_timer_init();        // 启动蜂鸣器定时器
                    buzzer_tick_started = RT_TRUE;   // 蜂鸣器定时器标志置位
                }
                break;
            case SUB_TASK_4:            // 子任务2.4按钮
                if (!matrix_light_started) // 如果点阵显示未启动
                {
                    matrix_light_thread_init();      // 启动点阵显示
                    matrix_light_started = RT_TRUE; // 点阵显示标志置位
                }
                break;
            case SUB_TASK_EXIT:       // 任务2退出按钮
                if (seg_disp_started) // 如果数码管显示已启动
                {
                    SHOW_NONE;                       // 清除显示
                    rt_thread_mdelay(10);            // 给清除留点时间
                    rt_timer_delete(seg_disp_timer); // 结束数码管显示
                    seg_disp_started = RT_FALSE;     // 数码管显示标志清零
                }
                if (buzzer_tick_started) // 若蜂鸣器定时器已启动
                {
                    rt_timer_delete(buzzer_tick_timer); // 结束蜂鸣器定时器
                    buzzer_tick_started = RT_FALSE;     // 蜂鸣器定时器标志清0
                    sec = COUNT_DOWN;                   // 恢复倒计时秒数
                    BUZZER_OFF;                         // 关闭蜂鸣器
                }
                if (matrix_light_started) // 如果点阵显示未启动
                {
                    rt_thread_mdelay(10);
                    rt_timer_delete(matrix_light_thread);
                    matrix_light_started = RT_FALSE; // 点阵显示标志置位
                    LED_ALL_OFF();
                }
                break;
            default:
                break;
            }
        }
        rt_thread_mdelay(20); // 每级任务线程的挂起时间短一点
    }
}

/*----------------------------------------------------------------
 * 函数名称：task2_thread_init
 * 功    能：任务2子线程的创建与启动（初始化）
 * 入口参数：无
 * 返 回 值：0 --- 成功，-1 --- 失败
 * 说明描述：创建动态线程的方式
 *---------------------------------------------------------------*/
int task2_thread_init(void)
{
    // 创建任务2子线程，栈空间略大，优先级略高
    task2_thread = rt_thread_create("task2_thread",
                                    task2_thread_entry,
                                    RT_NULL,
                                    THREAD_STACK_SIZE * 2,
                                    THREAD_PRIORITY - 2,
                                    THREAD_TIMESLICE);

    // 创建成功就启动任务2子线程
    if (task2_thread != RT_NULL)
    {
        rt_thread_startup(task2_thread);
        rt_kprintf("task2_thread running.\n");
        return 0;
    }
    else
    {
        rt_kprintf("Create task2_thread failed.\n");
        return -1;
    }
}
