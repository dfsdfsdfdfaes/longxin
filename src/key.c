//------------------------------------------------------------
// 代码清单4：key.c
//------------------------------------------------------------

#include "ls1b.h"
#include "ls1b_gpio.h"
#include "key.h"

//-------------------------------------------------------
// 全局变量
//-------------------------------------------------------
bool key1_ctrl_flag = false; // 按键1控制标志，初始无效
bool key2_ctrl_flag = false; // 按键2控制标志，初始无效

/*----------------------------------------------------------
 * 函数名称：Key1_IRQHandler
 * 函数功能：按键1中断向量函数，改变标志即可
 * 入口参数：按照中断向量函数参数格式
 * 返 回 值：无
 * 说    明：硬件中断优先级高于所有线程，务必做到快进快出，
 *           这里仅改变控制标志，在线程里根据标志
 * --------------------------------------------------------*/
static void Key1_IRQHandler(int vector, void *arg)
{
	if (!gpio_read(KEY1)) // 检测到按下
	{
		ls1x_disable_gpio_interrupt(KEY1); // 先禁止GPIO中断
		key1_ctrl_flag = !key1_ctrl_flag;  // 控制标志取反
		ls1x_enable_gpio_interrupt(KEY1);  // 再使能GPIO中断，为下次准备
	}
}

/*----------------------------------------------------------
 * 函数名称：Key2_IRQHandler
 * 函数功能：按键2中断向量函数，改变标志即可
 * 入口参数：按照中断向量函数参数格式
 * 返 回 值：无
 * 说    明：硬件中断优先级高于所有线程，务必做到快进快出，
 *           这里仅改变控制标志，在线程里根据标志
 * --------------------------------------------------------*/
static void Key2_IRQHandler(int vector, void *arg)
{
	if (!gpio_read(KEY2)) // 检测到按下
	{
		ls1x_disable_gpio_interrupt(KEY2); // 先禁止GPIO中断
		key2_ctrl_flag = !key2_ctrl_flag;  // 控制标志取反
		ls1x_enable_gpio_interrupt(KEY2);  // 再使能GPIO中断，为下次准备
	}
}

/*---------------------------------------------------------
 * 函数名称：Key_IsrInit
 * 函数功能：按键端口（GPIO中断）初始化
 * 入口参数：无
 * 返 回 值：无
 * 说    明：GPIO下降沿产生中断，默认即输入模式
 * ------------------------------------------------------*/
void Key_IsrInit(void)
{
	// 下降沿触发中断向量函数Key1_IRQHandler
	ls1x_install_gpio_isr(KEY1, INT_TRIG_EDGE_DOWN, Key1_IRQHandler, NULL);
	ls1x_install_gpio_isr(KEY2, INT_TRIG_EDGE_DOWN, Key2_IRQHandler, NULL);
}
