//-------------------------------------------------------
// 代码清单2：修正后的 drv_pwm.h
//-------------------------------------------------------

#ifndef _DRV_PWM_H
#define _DRV_PWM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
#include <rtdevice.h>
#include <rthw.h>
#include "bsp.h"

/*
 * PWM devices *
 */
#ifdef BSP_USE_PWM0
struct rt_device rt_ls1x_pwm0;
#endif
#ifdef BSP_USE_PWM1
struct rt_device rt_ls1x_pwm1;
#endif
#ifdef BSP_USE_PWM2
struct rt_device rt_ls1x_pwm2;
#endif
#ifdef BSP_USE_PWM3
struct rt_device rt_ls1x_pwm3;
#endif

/*
 * IO control command, args is as pwm_cfg_t *
 */
#define IOCTL_PWM_START         0x01
#define IOCTL_PWM_STOP          0x02

/*
 * Function declaration *
 */
void rt_ls1x_pwm_install(void);
rt_err_t rt_pwm_control(struct rt_device *dev,
                        int               cmd,
                        void             *args);

extern const char *ls1x_pwm_get_device_name(void *pwm);

#ifdef __cplusplus
}
#endif

#endif //_DRV_PWM_H
